DROP TABLE IF EXISTS uloge CASCADE; 
DROP SEQUENCE IF EXISTS uloge_SEQ CASCADE;

DROP TABLE IF EXISTS zaposlenici CASCADE;
DROP SEQUENCE IF EXISTS zaposlenici_SEQ CASCADE;

DROP TABLE IF EXISTS adrese CASCADE;
DROP SEQUENCE IF EXISTS adrese_SEQ CASCADE;

DROP TABLE IF EXISTS teren CASCADE;
DROP SEQUENCE IF EXISTS teren_SEQ CASCADE;

DROP TABLE IF EXISTS usluzne_djelatnosti CASCADE;
DROP SEQUENCE IF EXISTS usluzne_djelatnosti_SEQ CASCADE;

DROP TABLE IF EXISTS grupe CASCADE;
DROP SEQUENCE IF EXISTS grupe_SEQ CASCADE;

DROP TABLE IF EXISTS radni_zadatci CASCADE;
DROP SEQUENCE IF EXISTS radni_zadatci_SEQ CASCADE;

DROP TABLE IF EXISTS zaposlenik_grupa CASCADE;

DROP TABLE IF EXISTS radni_sati CASCADE;

DROP TABLE IF EXISTS korisnici CASCADE;
DROP SEQUENCE IF EXISTS korisnici_SEQ CASCADE;

DROP TABLE IF EXISTS radni_zadatak_zaposlenik CASCADE;

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

CREATE SEQUENCE uloge_SEQ INCREMENT BY 1;
CREATE TABLE uloge (
    id int not null DEFAULT nextval('uloge_SEQ'),
    naziv VARCHAR(64) not null,
	
    CONSTRAINT uloge_pk PRIMARY KEY (id)
);


CREATE SEQUENCE zaposlenici_SEQ INCREMENT BY 1;
CREATE TABLE zaposlenici (
    id int not null DEFAULT nextval('zaposlenici_SEQ'),
    ime VARCHAR(64) not null,
    prezime VARCHAR(64) not null,
    radno_vrijeme_sati_poc int not null,
    radno_vrijeme_sati_kraj int not null,
    duljina_radnog_vremena int not null,
    slobodni_dani int not null,
    id_uloga int not null, 
    
    CONSTRAINT uloga_fk foreign key (id_uloga) REFERENCES uloge(id)
	ON DELETE CASCADE,
    CONSTRAINT zaposlenici_pk PRIMARY KEY (id)
);


CREATE SEQUENCE adrese_SEQ INCREMENT BY 1;
CREATE TABLE adrese (
    id int not null DEFAULT nextval('adrese_SEQ'),
    koordinate VARCHAR(64) not null,
	adresa VARCHAR(64) UNIQUE,
	

    CONSTRAINT adrese_pk PRIMARY KEY (id)
);


CREATE SEQUENCE teren_SEQ INCREMENT BY 1;
CREATE TABLE teren (
	datum date not null,
	id_adresa int not null,
	id_zaposlenik int not null,
    
	CONSTRAINT adresa_fk foreign key (id_adresa) references adrese(id)
	ON DELETE CASCADE,
	CONSTRAINT zaposlenik_fk foreign key (id_zaposlenik) references zaposlenici(id)
	ON DELETE CASCADE,
	CONSTRAINT teren_pk primary key (id_zaposlenik, datum)
);


CREATE SEQUENCE usluzne_djelatnosti_SEQ INCREMENT BY 1;
CREATE TABLE usluzne_djelatnosti (
	id int not null DEFAULT nextval('usluzne_djelatnosti_SEQ'),
	opis text not null,
	naziv varchar(64) not null,
	cijena_sata int,
	CONSTRAINT djelatnost_pk PRIMARY KEY ( id )
);


CREATE SEQUENCE grupe_SEQ INCREMENT BY 1;
CREATE TABLE grupe (
	id int not null DEFAULT nextval('grupe_SEQ'),
	naziv varchar(64) not null,
	id_voditelj int not null,
	id_djelatnost int not null,
	CONSTRAINT voditelj_fk FOREIGN KEY(id_voditelj) REFERENCES zaposlenici(id)
	ON DELETE CASCADE,
	CONSTRAINT djelatnost_fk FOREIGN KEY ( id_djelatnost ) REFERENCES usluzne_djelatnosti(id)
	ON DELETE CASCADE,
	CONSTRAINT grupa_pk PRIMARY KEY( id) 
);


CREATE SEQUENCE radni_zadatci_SEQ INCREMENT BY 1;
CREATE TABLE radni_zadatci (
	id int not null DEFAULT nextval('radni_zadatci_SEQ'),
	
	procijenjeni_trosak int not null,
	cijena_sata int not null,
	opis text not null,
	datum_poc date not null,
	obavljen boolean DEFAULT false,
	id_djelatnost int not null,
	id_zaposlenik int not null,
	CONSTRAINT voditelj_fk FOREIGN KEY(id_zaposlenik) REFERENCES zaposlenici(id)
	ON DELETE CASCADE,
	CONSTRAINT djelatnost_fk FOREIGN KEY ( id_djelatnost ) REFERENCES usluzne_djelatnosti(id)
	ON DELETE CASCADE,
	CONSTRAINT zadatak_pk PRIMARY KEY( id ) 
);

CREATE TABLE zaposlenik_grupa (
	id_zaposlenik int,
	id_grupa int,
	CONSTRAINT zaposlenik_fk FOREIGN KEY(id_zaposlenik) REFERENCES zaposlenici(id)
	ON DELETE CASCADE,
	CONSTRAINT grupa_fk FOREIGN KEY(id_grupa) REFERENCES grupe(id)
	ON DELETE CASCADE,
	CONSTRAINT zaposlenik_grupa_pk PRIMARY KEY(id_zaposlenik, id_grupa)
);

CREATE TABLE radni_sati (
	id_zaposlenik int,
	id_radni_zadatak int,
	datum date not null,
	duljina_rada int not null,
	CONSTRAINT zaposlenik_fk FOREIGN KEY(id_zaposlenik) REFERENCES zaposlenici(id)
	ON DELETE CASCADE,
	CONSTRAINT radni_zadatak_fk FOREIGN KEY(id_radni_zadatak) REFERENCES radni_zadatci(id)
	ON DELETE CASCADE,
	CONSTRAINT radni_sati_pk PRIMARY KEY(id_zaposlenik, id_radni_zadatak, duljina_rada)
);

CREATE SEQUENCE korisnici_SEQ INCREMENT BY 1;
CREATE TABLE korisnici(
	id int not null DEFAULT nextval('korisnici_SEQ'),
	username varchar(64) UNIQUE not null,
	password varchar(256) not null,
	id_zaposlenik int not null,

	constraint korisnici_pk PRIMARY KEY(id),
	CONSTRAINT zaposlenik_fk FOREIGN KEY (id_zaposlenik) REFERENCES zaposlenici(id)
	ON DELETE CASCADE

);

CREATE TABLE radni_zadatak_zaposlenik (
    id_radni_zadatak int not null,
    id_zaposlenik int not null,

    CONSTRAINT radni_zadatak_zaposlenik_pk PRIMARY KEY(id_radni_zadatak, id_zaposlenik),
    CONSTRAINT radni_zadatak_fk FOREIGN KEY (id_radni_zadatak) REFERENCES radni_zadatci(id)
    ON DELETE CASCADE,
    CONSTRAINT zaposlenik_fk FOREIGN KEY (id_zaposlenik) REFERENCES zaposlenici(id)
    ON DELETE CASCADE
);

