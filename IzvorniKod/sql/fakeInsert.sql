DELETE FROM radni_sati;
DELETE FROM teren;
DELETE FROM adrese;
DELETE FROM radni_zadatci;
DELETE FROM zaposlenik_grupa;
DELETE FROM korisnici;
DELETE FROM grupe;
DELETE FROM zaposlenici;
DELETE FROM uloge;
DELETE FROM usluzne_djelatnosti;
DELETE FROM radni_zadatak_zaposlenik;

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

INSERT INTO usluzne_djelatnosti (naziv, opis, cijena_sata) 
    VALUES ('Trgovina', 'Nabavljanje i prodaja dobara.', 25);
INSERT INTO usluzne_djelatnosti (naziv, opis, cijena_sata) 
    VALUES ('Vođenje putovanja', 'Organizirati putovanje i ljudima opisati mjesto.', 28);
INSERT INTO usluzne_djelatnosti (naziv, opis, cijena_sata) 
    VALUES ('Ugostitelj', 'Iznajmljivanje prostora.', 35);
INSERT INTO usluzne_djelatnosti (naziv, opis, cijena_sata) 
    VALUES ('Odvjetništvo', 'Predstavljanje osobe na sudu.', 58);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

INSERT INTO uloge (naziv)
    VALUES ('ROLE_DIREKTOR');
INSERT INTO uloge (naziv)
    VALUES ('ROLE_VODITELJ');
INSERT INTO uloge (naziv)
    VALUES ('ROLE_DJELATNIK');
INSERT INTO uloge (naziv)
    VALUES ('ROLE_NULL');

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

INSERT INTO zaposlenici (ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Dino', 'Petrović', '1', 9, 17, 8, 14);
INSERT INTO zaposlenici (ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Margareta', 'Kasun', '2', 9, 17, 8, 14);
INSERT INTO zaposlenici  (ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Ratimir', 'Vlašić', '2', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Bogdan', 'Matić', '2', 9, 17, 8, 14);
INSERT INTO zaposlenici (ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Leo', 'Bogdanović', '2', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Alma', 'Dragović', '2', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Marta', 'Božić', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Mira', 'Stankić', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES  ('Vjera', 'Vlašić', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Valentina', 'Perko', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Ambrozije', 'Antić', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Tadija', 'Vukoja', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Mirna', 'Marić', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Aleksandar', 'Živković', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Borko', 'Župan', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Slavomir', 'Jerković', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Valent', 'Babić', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Marina', 'Tadić', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ( 'Ranko', 'Pavlović', '3', 9, 17, 8, 14);
INSERT INTO zaposlenici ( ime, prezime, id_uloga, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani)
    VALUES ('Terezija', 'Lučić', '3', 9, 17, 8, 14);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'DPetrovic', '$2a$10$aG4dsQ4wdoTL1c3uR8X1c.hpSl1P7FoGlsKQdU9OE5E6fSiG9dp2S', 1);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'MarKasun', '$2a$10$lLTx6SXu947gASEuAP/CgusXODfi.aICFWFLsIqIqKgTCs.xnolre', 2);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'RVlasic', '$2a$10$o3mrxcFkIj6Je44tYiVtkOGNxO2F/TSVCma1uHPUAaYB/hj/9bTEq', 3);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'BMatic', '$2a$10$bU3530ngd60S71kqWhfj5.KaZW0H3LZ73XKQprW5qCDx3zL6B40uG', 4);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'LeoBogdan', '$2a$10$gs5zHrHrrseqC0AHRn7SxOctUF9X0WyRs.QET65vYCw3owopS/Ehe', 5);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'xX_AlmaDragovic_Xx', '$2a$10$oPRwc6NDgnGNW887zRqfceDuNBi0u2E8Xb9VvIAHcD2PUM7gQgUiK', 6);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'MBozic', '$2a$10$JOnwlZL0Ak4efnQ0fCddZOXPam5QWjcCRoLSd1Jlnj34M.dQbIjx6', 7);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'StankaMiric', '$2a$10$ZCDTZIFeA8EGyfWFY8DdeuK2msyat8GQAfhLt8EoR8/zT0.LveseS', 8);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'VjeraVlasic', '$2a$10$93C1V8WBPjS6GrH1.2Hpo.hAVgCAVMeyX/aaXdoEK2NinuiHSS4Ne', 9);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'ValentinaPerko', '$2a$10$pXADQAVLCjsLgoLD0lgcWOUcXAr0Va9UqhgW7jrU7aU1LOYwHttWe', 10);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'AmbrozAnte', '$2a$10$VHn8pB7Ia20mYfnHZpmHXeBjHEmD0YxSR0LN6NjdTChTaAAQtVW5u', 11);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'TadijaVukoja', '$2a$10$MWkVipKnAa5Vn0I7rjP0h.uXoZCXKhx4rLWD89obrcgSgAfoxeGmm', 12);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'MirnaMara', '$2a$10$U8D.lhVaZjUiFIqwxIY7IODA/8vmZp4kSycIJ0bn3A2vLvazF8IoG', 13);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'AleksandarZiva', '$2a$10$qdqcIpOtJZgN7xXNx1NP7O.0zmS0RaHqGCNVnRe0/KWGSwWNuRGti', 14);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'BorkoZupo', '$2a$10$519TT2CAtJipWCp9YJ4d6OZJw7dT5U55KJ6znKxJEQHTXZyFdeeuu', 15);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'SlavoJerko', '$2a$10$GoAt3PpJ4hQR9jnpLFFSzu341J71s9O8GZbYjyQnBx66e21qcgSIS', 16);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'ValentBabinjo', '$2a$10$sOjh0mjrhTSbTFy9JZT./.BTZnUQPBTGOiTiFYnwRqstnWn0cIuSy', 17);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'MariTadi', '$2a$10$6VoZ2wmiCQlSzFE22QS0LOKKk9/3c45yiLMysvNDjJ4xrbelElfbm', 18);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'RankoPavlo', '$2a$10$bDgnEjxGbrxkGOn59B8opO6K5OOmTzcHE0RUTOydXSwZzZCXphnTq', 19);
INSERT INTO korisnici ( username, password, id_zaposlenik)
    VALUES ( 'TereLuce', '$2a$10$SNkW4msan9p8fHEQ7sHQz.LAF9R13tJS53HSIOIk7jRXHr46LfyGS', 20);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

INSERT INTO grupe ( id_voditelj, id_djelatnost, naziv)
    VALUES ( 2, 1, 'Trgovci');
INSERT INTO grupe ( id_voditelj, id_djelatnost, naziv)
    VALUES ( 3, 2, 'Voditelji putovanja');
INSERT INTO grupe ( id_voditelj, id_djelatnost, naziv)
    VALUES ( 4, 3, 'Ugostitelji');
INSERT INTO grupe ( id_voditelj, id_djelatnost, naziv)
    VALUES ( 5, 4, 'Odvjetnici');

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (1, 7);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (1, 8);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (1, 9);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (1, 10);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (2, 11);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (2, 12);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (3, 13);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (3, 14);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (3, 15);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (3, 16);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (4, 17);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (4, 18);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (4, 19);
INSERT INTO zaposlenik_grupa (id_grupa, id_zaposlenik)
    VALUES (4, 20);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

INSERT INTO radni_zadatci ( id_djelatnost, id_zaposlenik, procijenjeni_trosak, cijena_sata, opis, datum_poc)
    VALUES ( 1, 8, 75, 25, 'Dovesti artikle iz skladišta.', '2021-10-31');
INSERT INTO radni_zadatci ( id_djelatnost, id_zaposlenik, procijenjeni_trosak, cijena_sata, opis, datum_poc)
    VALUES ( 1, 9, 200, 25, 'Raditi smjenu na blagajni.', '2021-11-1');
INSERT INTO radni_zadatci ( id_djelatnost, id_zaposlenik, procijenjeni_trosak, cijena_sata, opis, datum_poc)
    VALUES ( 2, 11, 112, 28, 'Organizirati put za grupu srednjoškolaraca.', '2021-10-28');
INSERT INTO radni_zadatci ( id_djelatnost, id_zaposlenik, procijenjeni_trosak, cijena_sata, opis, datum_poc)
    VALUES ( 3, 13, 35, 35, 'Pospremiti sobu broj 202.', '2021-10-28');
INSERT INTO radni_zadatci ( id_djelatnost, id_zaposlenik, procijenjeni_trosak, cijena_sata, opis, datum_poc)
    VALUES ( 3, 14, 280, 35, 'Odraditi smjenu na porti.', '2021-10-29');
INSERT INTO radni_zadatci ( id_djelatnost, id_zaposlenik, procijenjeni_trosak, cijena_sata, opis, datum_poc)
    VALUES ( 3, 15, 105, 35, 'Pospremiti hodnike.', '2021-10-28');
INSERT INTO radni_zadatci ( id_djelatnost, id_zaposlenik, procijenjeni_trosak, cijena_sata, opis, datum_poc)
    VALUES ( 4, 17, 116, 58, 'Razgovarati s klijentom.', '2021-11-13');
INSERT INTO radni_zadatci ( id_djelatnost, id_zaposlenik, procijenjeni_trosak, cijena_sata, opis, datum_poc)
    VALUES ( 4, 17, 116, 58, 'Razgovarati s klijentom.', '2021-11-13');
INSERT INTO radni_zadatci ( id_djelatnost, id_zaposlenik, procijenjeni_trosak, cijena_sata, opis, datum_poc)
    VALUES ( 4, 19, 232, 58, 'Predstavljati klijenta na sudu', '2021-11-23');

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

INSERT INTO adrese ( koordinate, adresa)
    VALUES ( '45.801186843027374, 15.971359031371374', 'Unska 3');
INSERT INTO adrese ( koordinate, adresa)
    VALUES ( '45.82232639231357, 16.037723343161893', 'Ul. Dragutina Mandla 1');
INSERT INTO adrese ( koordinate, adresa)
    VALUES ( '45.806882062155196, 15.966266824479959', 'Kršnjavog 1');

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

INSERT INTO teren (id_zaposlenik, id_adresa, datum)
    VALUES (12, 3, '2021-11-1');

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

INSERT INTO radni_sati (id_zaposlenik, id_radni_zadatak, duljina_rada, datum)
    VALUES (11, 3, 5, '2021-10-28');
INSERT INTO radni_sati (id_zaposlenik, id_radni_zadatak, duljina_rada, datum)
    VALUES (13, 4, 1, '2021-10-28');
INSERT INTO radni_sati (id_zaposlenik, id_radni_zadatak, duljina_rada, datum)
    VALUES (15, 6, 3, '2021-10-28');

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

INSERT INTO radni_zadatak_zaposlenik (id_radni_zadatak, id_zaposlenik)
    VALUES (1, 7);
INSERT INTO radni_zadatak_zaposlenik (id_radni_zadatak, id_zaposlenik)
    VALUES (2, 8);
INSERT INTO radni_zadatak_zaposlenik (id_radni_zadatak, id_zaposlenik)
    VALUES (3, 11);
INSERT INTO radni_zadatak_zaposlenik (id_radni_zadatak, id_zaposlenik)
    VALUES (4, 13);
INSERT INTO radni_zadatak_zaposlenik (id_radni_zadatak, id_zaposlenik)
    VALUES (5, 14);
INSERT INTO radni_zadatak_zaposlenik (id_radni_zadatak, id_zaposlenik)
    VALUES (6, 15);
INSERT INTO radni_zadatak_zaposlenik (id_radni_zadatak, id_zaposlenik)
    VALUES (7, 17);
INSERT INTO radni_zadatak_zaposlenik (id_radni_zadatak, id_zaposlenik)
    VALUES (8, 18);
INSERT INTO radni_zadatak_zaposlenik (id_radni_zadatak, id_zaposlenik)
    VALUES (9, 18);

