import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Adresa } from '../model/Adresa';
import { BaseService } from '../shared/base-service.service';

@Injectable({
  providedIn: 'root'
})
export class AdresaService extends BaseService{

  constructor(httpClient : HttpClient) {
    super(httpClient);
  }

  getAll(): Observable<Adresa[]> {
    return this.get('/adresa');
  }

  getAllAdress(id: number): Observable<Adresa[]> {
    return this.get('/adresaZaposlenika/' + id);
  }
  getById(id: number): Observable<Adresa> {
    return this.get('/adresa/' + id);
  }

  getByIdZaposlenik(id: number): Observable<Adresa> {
    return this.get('/adresa/zaposlenik/' + id);
  }

  saveAdresa(adresa: Adresa): Observable<number> {
    return this.post('/adresa', adresa);
  }

  updateAdresa(adresa: Adresa): Observable<number> {
    return this.put('/adresa', adresa);
  }

  deleteAdresa(id: number): Observable<number> {
    return this.delete('/adresa/' + id);
  }
}
