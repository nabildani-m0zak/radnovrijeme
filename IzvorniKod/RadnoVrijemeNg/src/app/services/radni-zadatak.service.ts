import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RadniZadatak } from '../model/RadniZadatak';
import { BaseService } from '../shared/base-service.service';

@Injectable({
  providedIn: 'root'
})
export class RadniZadatakService extends BaseService {

  constructor(_httpClient: HttpClient) { 
    super(_httpClient);
  }

  getAllByDjelatnikId(id: number): Observable<RadniZadatak[]> {
    return this.get('/radniZadatak/djelatnik/' + id);
  }

  getAllObavljeniByDjelatnikId(id: number): Observable<RadniZadatak[]> {
    return this.get('/obavljenRadniZadatak/zaposlenik/' + id);
  }

  getAllByVoditeljId(id: number): Observable<RadniZadatak[]> {
    return this.get('/radniZadatak/voditelj/' + id);
  }

  getById(id: number): Observable<RadniZadatak> {
    return this.get('/radniZadatak/' + id);
  }

  update(id_zadatak: number, id_djelatnik: number): Observable<number> {
    return this.put('/radniZadatak/' + id_zadatak + '/djelatnik/' + id_djelatnik, {});
  }

  updateObavljen(id: number): Observable<number> {
    return this.put("/radniZadatak/" + id, {});
  }

  save(zadatak: RadniZadatak, id_zaposlenik: number): Observable<number> {
    return this.post('/radniZadatak/zaposlenik/' + id_zaposlenik, zadatak);
  }

  deleteZadatak(id: number): Observable<number> {
    return this.delete('/radniZadatak/' + id);
  }

  getRealiziraniTrosak(): Observable<number> {
    return this.get('/realiziraniTrosak')
  }

  getPlaniraniTrosak(): Observable<number> {
    return this.get('/planiraniTrosak')
  }
}
