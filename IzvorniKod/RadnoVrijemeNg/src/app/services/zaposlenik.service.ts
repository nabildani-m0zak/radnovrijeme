import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Zaposlenik } from '../model/Zaposlenik';
import { BaseService } from '../shared/base-service.service';

@Injectable({
  providedIn: 'root'
})
export class ZaposlenikService extends BaseService {

  constructor(_httpClient: HttpClient) { 
    super(_httpClient);
  }

  getAll(): Observable<Zaposlenik[]> {
    return this.get('/zaposlenik');
  }

  getById(id: number): Observable<Zaposlenik> {
    return this.get('/zaposlenik/' + id);
  }

  getAllVoditelji(): Observable<Zaposlenik[]> {
    return this.get('/voditelj');
  }

  getAllZaposleniciByVoditeljId(id: number): Observable<Zaposlenik[]> {
    return this.get('/zaposlenik/voditelj/' + id);
  }
  
  getVoditeljByDjelatnostId(id: number): Observable<Zaposlenik> {
    return this.get('/voditelj/djelatnost/' + id);
  }

  getDjelatnikByRadniZadatakId(id: number): Observable<Zaposlenik> {
    return this.get('/djelatnik/radnizadatak/' + id);
  }

  asignVoditeljToDjelatnost(id_djelatnost: number, id_voditelj: number): Observable<number> {
    return this.post('/usluznaDjelatnost/' + id_djelatnost + '/voditelj/' + id_voditelj, null);
  }

  saveZaposlenik(zaposlenik: Zaposlenik): Observable<number> {
    return this.post('/zaposlenik', zaposlenik);
  }

  asignZaposlenikToVoditelj(id_zaposlenik: number, id_voditelj: number): Observable<number> {
    return this.post('/zaposlenik/' + id_zaposlenik + '/voditelj/' + id_voditelj, {});
  }

  updateZaposlenik(zaposlenik: Zaposlenik): Observable<number> {
    return this.put('/zaposlenik', zaposlenik);
  }

  deleteZaposlenik(id: number): Observable<number> {
    return this.delete('/zaposlenik/' + id);
  }

  removeZaposlenikFromGrupa(idZaposlenik: number, idDjelatnost: number): Observable<number> {
    return this.delete('ukloni/zaposlenik/grupa' + idZaposlenik);
  }

  getZaposlenikZauzetost(idZaposlenik: number) : Observable<number>{ 
    return this.get('/zaposlenik/zauzetost/' + idZaposlenik);
  }

}
