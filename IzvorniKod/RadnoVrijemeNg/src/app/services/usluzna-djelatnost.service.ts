import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UsluznaDjelatnost } from '../model/UsluznaDjelatnost';
import { Zaposlenik } from '../model/Zaposlenik';
import { BaseService } from '../shared/base-service.service';

@Injectable({
  providedIn: 'root'
})
export class UsluznaDjelatnostService extends BaseService{

  constructor(_httpClient: HttpClient) { 
    super(_httpClient);
  }

  getAllUsluznaDjelatnost(): Observable<UsluznaDjelatnost[]>{
    return this.get('/usluznaDjelatnost');
  }

  getById(id: number): Observable<UsluznaDjelatnost> {
    return this.get('/usluznaDjelatnost/' + id);
  }

  getByVoditeljId(id: number): Observable<UsluznaDjelatnost> {
    return this.get('/usluznaDjelatnost/voditelj/' + id);
  }

  saveUsluznaDjelatnost(djelatnost: UsluznaDjelatnost): Observable<number> {
    return this.post('/usluznaDjelatnost', djelatnost);
  }

  updateUsluznaDjelatnost(djelatnost: UsluznaDjelatnost): Observable<number> {
    return this.put('/usluznaDjelatnost', djelatnost);
  }

  deleteUsluznaDjelatnost(id: number) {
    return this.delete('/usluznaDjelatnost/' + id);
  }

  getAllZaposlenici(id: number): Observable<Zaposlenik[][]>{
    return this.get('/zaposlenik/bygroup/' + id)
  }

 getRealiziraniTrosak(idVoditelj: number) : Observable<number>{
    return this.get('/realiziraniTrosak/' + idVoditelj);
  }

  getProcjenjeniTrosak(idVoditelj: number) : Observable<number> {
    return this.get('/planiraniTrosak/' + idVoditelj);
  } 
}
