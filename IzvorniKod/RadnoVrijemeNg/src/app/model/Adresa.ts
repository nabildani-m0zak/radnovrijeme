export class Adresa {
    /**
     * Id adrese
     */
    id:number;

    /**
     * Koordinate adrese
     */
    koordinate: string;

    /**
     * Puni naziv adrese
     */
    adresa: string;
}