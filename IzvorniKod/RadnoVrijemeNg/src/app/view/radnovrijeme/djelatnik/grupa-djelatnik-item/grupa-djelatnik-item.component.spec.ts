import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrupaDjelatnikItemComponent } from './grupa-djelatnik-item.component';

describe('GrupaDjelatnikItemComponent', () => {
  let component: GrupaDjelatnikItemComponent;
  let fixture: ComponentFixture<GrupaDjelatnikItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrupaDjelatnikItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrupaDjelatnikItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
