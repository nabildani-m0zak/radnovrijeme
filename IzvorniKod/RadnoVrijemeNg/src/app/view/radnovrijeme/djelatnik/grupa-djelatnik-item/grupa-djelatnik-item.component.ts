import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsluznaDjelatnost } from 'src/app/model/UsluznaDjelatnost';
import { Zaposlenik } from 'src/app/model/Zaposlenik';
import { LoginService } from 'src/app/services/login.service';
import { UsluznaDjelatnostService } from 'src/app/services/usluzna-djelatnost.service';
import { ZaposlenikService } from 'src/app/services/zaposlenik.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-grupa-djelatnik-item',
  templateUrl: './grupa-djelatnik-item.component.html',
  styleUrls: ['./grupa-djelatnik-item.component.css']
})
export class GrupaDjelatnikItemComponent extends BaseComponent implements OnInit {

  @Input() grupa: UsluznaDjelatnost
  voditelj: Zaposlenik;

  constructor(private login: LoginService,
      private voditeljService: ZaposlenikService,
      private grupeService: UsluznaDjelatnostService,
      private router: Router) { 
    super(login);
  }

  childNgOnInit() {
    this.grupa = new UsluznaDjelatnost(this.grupa[0], this.grupa[1],this.grupa[2],this.grupa[3])
    
    this.voditeljService.getVoditeljByDjelatnostId(this.grupa.id).subscribe(res => {
      if (res) {
        this.voditelj = res;
    }
    })
  }

  pregledZaposlenika() {
    this.router.navigate(['grupa-kolege', this.grupa.id]);
  }

}
