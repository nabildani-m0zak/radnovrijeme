import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GrupaKolegeRoutingModule } from './grupa-kolege-routing.module';
import { GrupaKolegeComponent } from './grupa-kolege.component';
import { ForbiddenPanelModule } from '../../forbidden-panel/forbidden-panel.module';


@NgModule({
  declarations: [GrupaKolegeComponent],
  imports: [
    CommonModule,
    ForbiddenPanelModule,
    GrupaKolegeRoutingModule
  ],
  exports: [GrupaKolegeComponent]
})
export class GrupaKolegeModule { }
