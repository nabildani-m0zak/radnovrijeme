import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrupaKolegeComponent } from './grupa-kolege.component';

describe('GrupaKolegeComponent', () => {
  let component: GrupaKolegeComponent;
  let fixture: ComponentFixture<GrupaKolegeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrupaKolegeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrupaKolegeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
