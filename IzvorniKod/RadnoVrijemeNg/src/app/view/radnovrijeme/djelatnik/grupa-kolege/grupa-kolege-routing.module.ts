import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrupaKolegeComponent } from './grupa-kolege.component';

const routes: Routes = [
  {
    path: '', component: GrupaKolegeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GrupaKolegeRoutingModule { }
