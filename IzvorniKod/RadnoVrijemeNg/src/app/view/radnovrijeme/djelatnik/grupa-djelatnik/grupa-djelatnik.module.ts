import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GrupaDjelatnikRoutingModule } from './grupa-djelatnik-routing.module';
import { GrupaDjelatnikComponent } from './grupa-djelatnik.component';
import { ForbiddenPanelModule } from '../../forbidden-panel/forbidden-panel.module';
import { GrupaDjelatnikItemComponent } from '../grupa-djelatnik-item/grupa-djelatnik-item.component';


@NgModule({
  declarations: [GrupaDjelatnikComponent, GrupaDjelatnikItemComponent],
  imports: [
    CommonModule,
    ForbiddenPanelModule,
    GrupaDjelatnikRoutingModule
  ],
  exports: [GrupaDjelatnikComponent]
})
export class GrupaDjelatnikModule { }
