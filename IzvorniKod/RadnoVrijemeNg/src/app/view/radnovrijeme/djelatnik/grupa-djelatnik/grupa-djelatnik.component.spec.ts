import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrupaDjelatnikComponent } from './grupa-djelatnik.component';

describe('GrupaDjelatnikComponent', () => {
  let component: GrupaDjelatnikComponent;
  let fixture: ComponentFixture<GrupaDjelatnikComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrupaDjelatnikComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrupaDjelatnikComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
