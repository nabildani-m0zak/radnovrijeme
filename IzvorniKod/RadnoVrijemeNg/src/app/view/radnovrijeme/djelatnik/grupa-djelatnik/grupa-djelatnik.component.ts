import { emitDistinctChangesOnlyDefaultValue } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { RadniZadatak } from 'src/app/model/RadniZadatak';
import { UsluznaDjelatnost } from 'src/app/model/UsluznaDjelatnost';
import { Zaposlenik } from 'src/app/model/Zaposlenik';
import { LoginService } from 'src/app/services/login.service';
import { RadniZadatakService } from 'src/app/services/radni-zadatak.service';
import { UsluznaDjelatnostService } from 'src/app/services/usluzna-djelatnost.service';
import { ZaposlenikService } from 'src/app/services/zaposlenik.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-grupa-djelatnik',
  templateUrl: './grupa-djelatnik.component.html',
  styleUrls: ['./grupa-djelatnik.component.css']
})
export class GrupaDjelatnikComponent extends BaseComponent implements OnInit {
  
  djelatnik: Zaposlenik;
  radniZadaci: RadniZadatak[];
  grupe: UsluznaDjelatnost[] = [];
  voditelj: Zaposlenik[];
  brojac = 0
  brojac_i = 0
  boolean_nema = false

  constructor(private loginService : LoginService,
    private _zaposlenikService: ZaposlenikService,
    private _radniZadatakService: RadniZadatakService,
    private grupeService: UsluznaDjelatnostService
    ) { 
      super(loginService);

      //this.djelatnik = new Zaposlenik();
    }

  childNgOnInit() {

    //radni zadatci
    if(this.appUser.role == 'ROLE_DJELATNIK') {
      this._radniZadatakService.getAllByDjelatnikId(this.appUser.id).subscribe(
        res => {
          this.radniZadaci = res;
          this.grupeService.getAllUsluznaDjelatnost().subscribe(res2 => {
            if (res2) {
              for(var i of res2){
                //this.brojac_i++;
                //usporedujes id svih djelatnosti sa onima koje su vezane za tog djelatnika
                for(var j of res){
                  if(i[0] == j.id_djelatnost){
                    this.grupe[this.brojac] = res2[j.id_djelatnost - 1]
                    this.brojac++
                  }
                }
              }
            }
         });
        }
      );
    }
  }
}
