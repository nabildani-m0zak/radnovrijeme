import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrupaDjelatnikComponent } from './grupa-djelatnik.component';

const routes: Routes = [
  {
    path: '', component: GrupaDjelatnikComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GrupaDjelatnikRoutingModule { }
