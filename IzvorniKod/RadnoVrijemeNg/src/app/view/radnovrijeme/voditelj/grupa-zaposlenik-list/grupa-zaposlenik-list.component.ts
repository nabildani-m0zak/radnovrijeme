import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Zaposlenik } from 'src/app/model/Zaposlenik';
import { LoginService } from 'src/app/services/login.service';
import { UlogaService } from 'src/app/services/uloga.service';
import { ZaposlenikService } from 'src/app/services/zaposlenik.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-grupa-zaposlenik-list',
  templateUrl: './grupa-zaposlenik-list.component.html',
  styleUrls: ['./grupa-zaposlenik-list.component.css']
})
export class GrupaZaposlenikListComponent extends BaseComponent implements OnInit {

  //zaposlenici koji ne pripadaju grupi prijavljenog voditelja
  zaposlenici: Zaposlenik[];
  idDjelatnikUloga: number;
  msg = '';

  constructor(private _loginService: LoginService,
             private _zaposlenikService: ZaposlenikService,
             private _ulogaService: UlogaService,
             private router: Router) { 
    super(_loginService);
  }

  childNgOnInit(): void {
    let id = this.appUser.id;
    let allZaposlenici: Zaposlenik[];

    this._ulogaService.getAll().subscribe( res => {
      this.idDjelatnikUloga = res.filter( u => u.naziv == 'ROLE_DJELATNIK')[0][0];

      this._zaposlenikService.getAll().subscribe( res => {
        allZaposlenici = res;
  
        this._zaposlenikService.getAllZaposleniciByVoditeljId(id).subscribe( 
          res => { 
            //svi id-ovi od zaposlenika koji pripadaju voditelju
            let indexArr: number[] = [];
            for(let i = 0; i < res.length;  i++) {
              indexArr.push(res[i][0]);
            }
  
            this.zaposlenici = allZaposlenici.filter(x => !indexArr.includes(x[0]) && x[7] == this.idDjelatnikUloga);
          }
        );
      })
    })
  }

  onZaposlenikSelect($event) {
    this._zaposlenikService.asignZaposlenikToVoditelj($event, this.appUser.id).subscribe( res => {
      if(res) {
        window.scrollTo({ top: 0, behavior: 'smooth' });
        
        this.msg = 'Zaposlenik uspješno pridijeljen grupi. Proslijeđujemo vas na popis zaposlenika.';

        setTimeout(() => {
          this.router.navigateByUrl('/');
        }, 4000);
      }
    })
  }
}
