import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RadniZadatak } from 'src/app/model/RadniZadatak';
import { Zaposlenik } from 'src/app/model/Zaposlenik';
import { LoginService } from 'src/app/services/login.service';
import { RadniZadatakService } from 'src/app/services/radni-zadatak.service';
import { UsluznaDjelatnostService } from 'src/app/services/usluzna-djelatnost.service';
import { ZaposlenikService } from 'src/app/services/zaposlenik.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-voditelj-zadaci-form',
  templateUrl: './voditelj-zadaci-form.component.html',
  styleUrls: ['./voditelj-zadaci-form.component.css']
})
export class VoditeljZadaciFormComponent extends BaseComponent implements OnInit {

  zadatak: RadniZadatak = new RadniZadatak();
  zaposlenici: Zaposlenik[];
  id_zaposlenik: number = 0;
  errMsg = '';
  successMsg = '';

  constructor(private _loginService: LoginService,
              private _radniZadatakService: RadniZadatakService,
              private _zaposlenikService: ZaposlenikService,
              private _usluznaDjelatnostService: UsluznaDjelatnostService,
              private router: Router) { 
    super(_loginService);
  }

  childNgOnInit(): void {
    this.zadatak.id_zaposlenik = this.appUser.id;

    this._usluznaDjelatnostService.getByVoditeljId(this.appUser.id).subscribe( res => {
      if(!res.id) {
        this.errMsg = 'Nemože se kreirati zadatak ukoliko niste pridijeljeni nijednoj djelatnosti.';
        return;
      }

      this.zadatak.id_djelatnost = res.id;
    });

    this._zaposlenikService.getAllZaposleniciByVoditeljId(this.appUser.id).subscribe( res => {
      if(res) {
        this.zaposlenici = res;
      }
    })
  }
  

  /**
   * Provjerava ispravnost podataka pomoću metode <code>validate</code>, sprema podatke na poslužitelj te ovisno u uspješnosti
   * spremanja podataka ispisuje odgovarajuću poruku.
   */
  submit() {
    this.successMsg = '';
    this.errMsg = '';

    if(this.validate()) {
      this._radniZadatakService.save(this.zadatak, this.id_zaposlenik).subscribe( 
        res => {
          if(res < 0) {
            this.errMsg = 'Pogreška prilikom spremanja podataka. Pokušajte ponovo.'
          }

          this.successMsg = 'Podatci uspješno spremljeni.'
        }
      );
    } 
  }

  /**
   * Provjerava ispravnost unesenih podataka u formi.
   * @returns istina ako su podaci ispravno uneseni, laž inače
   */
  validate(): boolean {
    if(!this.zadatak.opis || !this.zadatak.id_djelatnost ||
      !this.id_zaposlenik || !this.zadatak.procijenjeni_trosak || !this.zadatak.datum_poc) {
      this.errMsg = 'Molimo unesite sve podatke.';
      return false;
    }

    return true;
  }

  convertDate(date: Date): string {
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();

    return `${year}-${month}-${day}`;
  }
}