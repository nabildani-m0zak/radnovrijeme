import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RadniSati } from 'src/app/model/RadniSati';
import { RadniZadatak } from 'src/app/model/RadniZadatak';
import { LoginService } from 'src/app/services/login.service';
import { RadniZadatakService } from 'src/app/services/radni-zadatak.service';
import { UpisSatiService } from 'src/app/services/upis-sati.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-upis-sati',
  templateUrl: './upis-sati.component.html',
  styleUrls: ['./upis-sati.component.css']
})
export class UpisSatiComponent extends BaseComponent implements OnInit {

  radniSati: RadniSati;
  radniZadaci: RadniZadatak[];
  errMsg = '';
  successMsg = '';

  constructor(private _loginService: LoginService,
              private _upisSatiService: UpisSatiService,
              private _radniZadatakService: RadniZadatakService,
              private router: Router) { 
    super(_loginService);
    this.radniSati = new RadniSati();
  }

  childNgOnInit(): void {
    this.radniSati.id_zaposlenik = this.appUser.id;
    
    if(this.appUser.role == 'ROLE_DJELATNIK') {
      this._radniZadatakService.getAllByDjelatnikId(this.appUser.id).subscribe(
        res => {
          this.radniZadaci = res;
        }
      );
    }
  }  

  /**
   * Provjerava ispravnost podataka pomoću metode <code>validate</code>, sprema podatke na poslužitelj te ovisno u uspješnosti
   * spremanja podataka ispisuje odgovarajuću poruku.
   */
  submit() {
    this.successMsg = '';
    this.errMsg = '';

    if(this.validate()) {
      this._upisSatiService.saveRadniSati(this.radniSati).subscribe( 
        res => {
          if(res < 0) {
            this.errMsg = 'Pogreška prilikom spremanja podataka. Pokušajte ponovo.'
          }

          this.successMsg = 'Podatci uspješno spremljeni.'
        }
      );
    }
      
  }

  /**
   * Provjerava ispravnost unesenih podataka u formi.
   * @returns istina ako su podaci ispravno uneseni, laž inače
   */
  validate(): boolean {
    if(this.radniSati == null || this.radniSati === undefined) return false;

    if(
      (this.appUser.role == 'ROLE_DJELATNIK' && this.radniSati.id_zadatak <= 0) ||
        this.radniSati.id_zaposlenik <= 0 ||
        this.radniSati.sati <= 0 ||
        this.radniSati.id_zadatak <= 0) {
      this.errMsg = 'Molimo unesite sve podatke.'
      return false;
    }

    this.successMsg = 'Podatci uspješno spremljeni.'
    return true;
  }
}
