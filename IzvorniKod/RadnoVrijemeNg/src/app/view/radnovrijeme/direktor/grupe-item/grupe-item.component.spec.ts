import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrupeItemComponent } from './grupe-item.component';

describe('GrupeItemComponent', () => {
  let component: GrupeItemComponent;
  let fixture: ComponentFixture<GrupeItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrupeItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrupeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
