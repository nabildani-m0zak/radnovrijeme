import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LokacijaItemComponent } from './lokacija-item.component';

describe('LokacijaItemComponent', () => {
  let component: LokacijaItemComponent;
  let fixture: ComponentFixture<LokacijaItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LokacijaItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LokacijaItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
