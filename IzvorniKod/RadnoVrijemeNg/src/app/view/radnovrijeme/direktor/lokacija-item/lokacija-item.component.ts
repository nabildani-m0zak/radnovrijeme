import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Adresa } from 'src/app/model/Adresa';
import { LoginService } from 'src/app/services/login.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-lokacija-item',
  templateUrl: './lokacija-item.component.html',
  styleUrls: ['./lokacija-item.component.css']
})
export class LokacijaItemComponent extends BaseComponent implements OnInit {

  @Input() adresa: Adresa;

  constructor(private loginService: LoginService,
              private router: Router) {
    super(loginService)
   }

  childNgOnInit(): void {
   this.adresa.adresa = this.adresa[2];
  }

}
