import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Adresa } from 'src/app/model/Adresa';
import { Coordinates } from 'src/app/model/Coordinates';
import { LoginService } from 'src/app/services/login.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent extends BaseComponent implements OnInit {
  @Input() coordinates: Coordinates[]
  @Input() lat:number
  @Input() lng:number;
  @Input() adrese: Adresa[]

  constructor(private loginService: LoginService,
    private router: Router) {
    super(loginService);
   }

   childNgOnInit() {
     console.log(this.adrese);
   }


}



