import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrupaZaposleniciComponent } from './grupa-zaposlenici.component';

const routes: Routes = [
  {
    path: '', component: GrupaZaposleniciComponent 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GrupaZaposleniciRoutingModule{ }
