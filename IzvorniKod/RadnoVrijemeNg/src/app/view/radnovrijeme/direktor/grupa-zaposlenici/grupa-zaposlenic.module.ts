import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { ForbiddenPanelComponent } from "../../forbidden-panel/forbidden-panel.component";
import { ForbiddenPanelModule } from "../../forbidden-panel/forbidden-panel.module";
import { ZaposlenikComponent } from "../zaposlenik/zaposlenik.component";
import { GrupaZaposleniciRoutingModule } from "./grupa-zaposlenici-routing.module";
import { GrupaZaposleniciComponent } from "./grupa-zaposlenici.component";


@NgModule({
    declarations: [GrupaZaposleniciComponent],
    imports: [
        CommonModule,
        ForbiddenPanelModule,
        GrupaZaposleniciRoutingModule,
    ],
    exports: [GrupaZaposleniciComponent]
})
export class GrupaZaposleniciModule{}