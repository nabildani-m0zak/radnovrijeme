import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrupaZaposleniciComponent } from './grupa-zaposlenici.component';

describe('GrupaZaposleniciComponent', () => {
  let component: GrupaZaposleniciComponent;
  let fixture: ComponentFixture<GrupaZaposleniciComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GrupaZaposleniciComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrupaZaposleniciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
