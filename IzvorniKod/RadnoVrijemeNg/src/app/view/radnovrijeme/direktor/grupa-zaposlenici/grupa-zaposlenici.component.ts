import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Zaposlenik } from 'src/app/model/Zaposlenik';
import { LoginService } from 'src/app/services/login.service';
import { UsluznaDjelatnostService } from 'src/app/services/usluzna-djelatnost.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-grupa-zaposlenici',
  templateUrl: './grupa-zaposlenici.component.html',
  styleUrls: ['./grupa-zaposlenici.component.css']
})
export class GrupaZaposleniciComponent extends BaseComponent implements OnInit {
  groupId: number
  zaposlenici: Zaposlenik[]
  groupName: string;

  constructor(private loginService : LoginService,
              private route: ActivatedRoute,
              private usluznaDjelatnostService: UsluznaDjelatnostService) {
    super(loginService);
   }

  childNgOnInit() {
    this.groupId = this.route.snapshot.params['id'];
    
    this.usluznaDjelatnostService.getById(this.groupId).subscribe(res => {
      if (res) {
        this.groupName = res.naziv.toLowerCase();
      }
    })

    this.usluznaDjelatnostService.getAllZaposlenici(this.groupId).subscribe(res => {
      if (res) {
         this.zaposlenici = res[0];
         console.log(this.zaposlenici)
      }
    })
  }

}
