import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LokacijaPregledComponent } from './lokacija-pregled.component';

describe('LokacijaPregledComponent', () => {
  let component: LokacijaPregledComponent;
  let fixture: ComponentFixture<LokacijaPregledComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LokacijaPregledComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LokacijaPregledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
