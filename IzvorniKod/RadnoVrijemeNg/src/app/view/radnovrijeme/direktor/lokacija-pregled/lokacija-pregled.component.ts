import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Adresa } from 'src/app/model/Adresa';
import { AdresaService } from 'src/app/services/adresa.service';
import { LoginService } from 'src/app/services/login.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-lokacija-pregled',
  templateUrl: './lokacija-pregled.component.html',
  styleUrls: ['./lokacija-pregled.component.css']
})
export class LokacijaPregledComponent extends BaseComponent implements OnInit {

  adrese: Adresa[]

  constructor(private loginService : LoginService,
              private route: ActivatedRoute,
              private adresaService: AdresaService,
              private httpClient: HttpClient,
              private router: Router) {
      super(loginService)
     }

  childNgOnInit(){

      this.adresaService.getAll().subscribe( async res => {
        this.adrese = res
       }
      );
  }


}
