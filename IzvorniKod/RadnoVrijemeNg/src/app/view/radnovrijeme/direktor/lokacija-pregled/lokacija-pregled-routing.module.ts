import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LokacijaPregledComponent } from './lokacija-pregled.component';

const routes: Routes = [
  {
    path: '', component:LokacijaPregledComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LokacijaPregledRoutingModule { }
