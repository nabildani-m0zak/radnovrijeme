import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LokacijaPregledRoutingModule } from './lokacija-pregled-routing.module';
import { LokacijaPregledComponent } from './lokacija-pregled.component';
import { ForbiddenPanelModule } from '../../forbidden-panel/forbidden-panel.module';
import { LokacijaItemComponent } from '../lokacija-item/lokacija-item.component';


@NgModule({
  declarations: [
    LokacijaPregledComponent,
    LokacijaItemComponent
  ],
  imports: [
    CommonModule,
    ForbiddenPanelModule,
    LokacijaPregledRoutingModule
  ],
  exports: [LokacijaPregledComponent, LokacijaItemComponent]
})
export class LokacijaPregledModule { }
