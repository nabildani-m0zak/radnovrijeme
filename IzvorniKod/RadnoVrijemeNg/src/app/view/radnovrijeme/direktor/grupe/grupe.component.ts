import { Component, OnInit } from '@angular/core';
import { UsluznaDjelatnost } from 'src/app/model/UsluznaDjelatnost';
import { Zaposlenik } from 'src/app/model/Zaposlenik';
import { LoginService } from 'src/app/services/login.service';
import { RadniZadatakService } from 'src/app/services/radni-zadatak.service';
import { UsluznaDjelatnostService } from 'src/app/services/usluzna-djelatnost.service';
import { ZaposlenikService } from 'src/app/services/zaposlenik.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-grupe',
  templateUrl: './grupe.component.html',
  styleUrls: ['./grupe.component.css']
})
export class GrupeComponent extends BaseComponent implements OnInit {
  grupe: UsluznaDjelatnost[]
  realiziraniTrosak = 0
  planiraniTrosak = 0;

  constructor(private loginService : LoginService,
            private grupeService: UsluznaDjelatnostService,
            private zaposlenikService: ZaposlenikService,
            private radniZadatakService: RadniZadatakService) { 
    super(loginService);
  }

  childNgOnInit() {
    this.grupeService.getAllUsluznaDjelatnost().subscribe(res => {
       if (res) {
         this.grupe = res
       }
    });

    this.radniZadatakService.getPlaniraniTrosak().subscribe(res => {
        if (res) {
          this.planiraniTrosak = res;
        }
    })

    this.radniZadatakService.getRealiziraniTrosak().subscribe(res => {
        if (res) {
          console.log(res);
          this.realiziraniTrosak = res;
        }
    })

    console.log(this.planiraniTrosak);

  }


  }
