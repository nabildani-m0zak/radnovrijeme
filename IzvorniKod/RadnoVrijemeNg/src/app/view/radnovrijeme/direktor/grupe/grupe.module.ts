import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ForbiddenPanelModule } from "../../forbidden-panel/forbidden-panel.module";
import { GrupeRoutingModule } from "./grupe-routing.module";
import { GrupeComponent } from "./grupe.component";
import { GrupeItemComponent } from "../grupe-item/grupe-item.component";

@NgModule( {
    declarations: [GrupeComponent, GrupeItemComponent],
    imports: [
        CommonModule,
        ForbiddenPanelModule,
        GrupeRoutingModule,
    ],
    exports: [GrupeComponent, GrupeItemComponent]
})

export class GrupeModule{}