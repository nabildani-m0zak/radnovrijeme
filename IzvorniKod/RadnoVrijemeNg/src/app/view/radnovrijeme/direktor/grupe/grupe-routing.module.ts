import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrupeComponent } from './grupe.component';

const routes: Routes = [
  {
    path: '', component: GrupeComponent 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GrupeRoutingModule{ }