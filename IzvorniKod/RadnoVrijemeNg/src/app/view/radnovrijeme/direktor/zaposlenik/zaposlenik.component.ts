import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Zaposlenik } from 'src/app/model/Zaposlenik';
import { LoginService } from 'src/app/services/login.service';
import { ZaposlenikService } from 'src/app/services/zaposlenik.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-zaposlenik',
  templateUrl: './zaposlenik.component.html',
  styleUrls: ['./zaposlenik.component.css']
})
export class ZaposlenikComponent extends BaseComponent implements OnInit {

  @Input() zaposlenik: Zaposlenik;
  isForDelete = false;
  zauzetost: number = 0;
  //broj radnih dana u tjednu
  raspolozivost: number = 5;

  constructor(private _loginService: LoginService,
             private _zaposlenikService: ZaposlenikService,
              private router: Router) { 
    super(_loginService);
  }

  childNgOnInit(): void {
    this.zaposlenik.id = this.zaposlenik[0];
    this.zaposlenik.ime = this.zaposlenik[1];
    this.zaposlenik.prezime = this.zaposlenik[2];
    this.zaposlenik.radno_vrijeme_sati_poc = this.zaposlenik[3];
    this.zaposlenik.radno_vrijeme_sati_kraj = this.zaposlenik[4];
    this.zaposlenik.duljina_radnog_vremena = this.zaposlenik[5];
    this.zaposlenik.slobodni_dani = this.zaposlenik[6];
    this.zaposlenik.id_uloga = this.zaposlenik[7];

    this.raspolozivost *= this.zaposlenik.duljina_radnog_vremena;

    this._zaposlenikService.getZaposlenikZauzetost(this.zaposlenik.id).subscribe(res => {
      if (res) {
        this.zauzetost = res
        this.raspolozivost -= this.zauzetost;
      }
    }
    )
  }

  onZaposlenikSelect() {
    this.router.navigateByUrl('/zaposlenik/' + this.zaposlenik.id);
  }

  showLocation() {
    this.router.navigateByUrl('/lokacija/' + this.zaposlenik.id);
  }
}
