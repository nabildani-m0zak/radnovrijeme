import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Korisnik } from 'src/app/model/Korisnik';
import { Uloga } from 'src/app/model/Uloga';
import { Zaposlenik } from 'src/app/model/Zaposlenik';
import { KorisnikService } from 'src/app/services/korisnik.service';
import { LoginService } from 'src/app/services/login.service';
import { UlogaService } from 'src/app/services/uloga.service';
import { ZaposlenikService } from 'src/app/services/zaposlenik.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-zaposlenici-form',
  templateUrl: './zaposlenici-form.component.html',
  styleUrls: ['./zaposlenici-form.component.css']
})
export class ZaposleniciFormComponent extends BaseComponent implements OnInit {

  zaposlenik: Zaposlenik = new Zaposlenik();
  korisnik: Korisnik = new Korisnik();
  uloge: Uloga[];
  isForDelete = false;
  isEdit = false;
  errMsg = '';
  successMsg = '';

  constructor(private _loginService: LoginService,
              private _zaposlenikService: ZaposlenikService,
              private _korisnikService: KorisnikService,
              private _ulogaService: UlogaService,
              private activatedRoute: ActivatedRoute,
              private router: Router) { 
    super(_loginService);
  }

  childNgOnInit(): void {
    let id = +this.activatedRoute.snapshot.paramMap.get('id');     
    if(id) {
      this.isEdit = true;
      this._zaposlenikService.getById(id).subscribe( 
        res => {
          this.zaposlenik = res;
          this._korisnikService.getByIdZaposlenik(this.zaposlenik.id).subscribe( res => {
            this.korisnik = res;
            this.korisnik.password = '';
          })
        }
      )
    }

    this._ulogaService.getAll().subscribe( res => {
      this.uloge = res.filter(u => u[0] == 2 || u[0] == 3);
    })
  }  

  /**
   * Provjerava ispravnost podataka pomoću metode <code>validate</code>, sprema podatke na poslužitelj te ovisno u uspješnosti
   * spremanja podataka ispisuje odgovarajuću poruku.
   */
  submit() {
    this.successMsg = '';
    this.errMsg = '';

    if(!this.validate())
        return;

    if(!this.isEdit) {
      this._zaposlenikService.saveZaposlenik(this.zaposlenik).subscribe( 
        res => {
          if(res < 0) {
            this.errMsg = 'Pogreška prilikom spremanja podataka. Pokušajte ponovo.'
            return;
          }

          this.korisnik.id_zaposlenik = res;

          this._korisnikService.saveKorisnik(this.korisnik).subscribe( res => {
            if(res < 0) {
              this.errMsg = 'Pogreška prilikom spremanja podataka. Pokušajte ponovo.'
              return;
            }

            this.successMsg = 'Podatci uspješno spremljeni.'
          });
        }
      );
    } else {
      this._zaposlenikService.updateZaposlenik(this.zaposlenik).subscribe( 
        res => {
          if(res < 0) {
            this.errMsg = 'Pogreška prilikom spremanja podataka. Pokušajte ponovo.'
            return;
          }

          this._korisnikService.updateKorisnik(this.korisnik).subscribe( res => {
            if(res < 0) {
              this.errMsg = 'Pogreška prilikom spremanja podataka. Pokušajte ponovo.'
              return;
            }

            this.successMsg = 'Podatci uspješno spremljeni.'
          });
        }
      );
    }
  }

  onDelete() {
    this.errMsg = '';
    this.successMsg = '';
    
    this.isForDelete = true;
  }

  onConfirmDelete() {
    this._korisnikService.deleteKorisnik(this.korisnik.id).subscribe( res => {
      if(res < 0) {
        this.errMsg = 'Pogreška prilikom brisanja podataka. Pokušajte ponovo.'
        return;
      }

      this._zaposlenikService.deleteZaposlenik(this.zaposlenik.id).subscribe( res => {
        if(res < 0) {
          this.errMsg = 'Pogreška prilikom brisanja podataka. Pokušajte ponovo.'
          return;
        }
      })
    })

    this.successMsg = 'Podatci uspješno izbrisani. Preuslijeđujemo vas na popis zaposlenika.'

    setTimeout( () => {
      this.router.navigateByUrl('/zaposlenici');
    }, 3000)
  }

  onDiscardDelete() {
    this.isForDelete = false;
  }

  /**
   * Provjerava ispravnost unesenih podataka u formi.
   * @returns istina ako su podaci ispravno uneseni, laž inače
   */
  validate(): boolean {
    if(!this.zaposlenik.duljina_radnog_vremena || !this.zaposlenik.id_uloga || !this.zaposlenik.ime || !this.zaposlenik.prezime ||
       !this.zaposlenik.radno_vrijeme_sati_kraj || !this.zaposlenik.radno_vrijeme_sati_poc || !this.zaposlenik.slobodni_dani ||
       !this.korisnik.password || !this.korisnik.username) {
      this.errMsg = 'Molimo unesite sve podatke.';
      return false;
    }

    if(this.zaposlenik.radno_vrijeme_sati_poc > this.zaposlenik.radno_vrijeme_sati_kraj) {
      this.errMsg = 'Početak radnog vremena ne može biti veći od kraja.';
      return false;
    }

    return true;
  }
}

