import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'

import { LokacijaFormRoutingModule } from './lokacija-form-routing.module';
import { ForbiddenPanelModule } from '../../forbidden-panel/forbidden-panel.module';
import { LokacijaFormComponent } from './lokacija-form.component';


@NgModule({
  declarations: [LokacijaFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ForbiddenPanelModule,
    LokacijaFormRoutingModule
  ],
  exports: [LokacijaFormComponent]
})
export class LokacijaFormModule { }