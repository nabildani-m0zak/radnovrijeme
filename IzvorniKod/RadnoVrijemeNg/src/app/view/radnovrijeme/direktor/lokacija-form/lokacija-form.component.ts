import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Adresa } from 'src/app/model/Adresa';
import { Coordinates } from 'src/app/model/Coordinates';
import { AdresaService } from 'src/app/services/adresa.service';
import { LoginService } from 'src/app/services/login.service';
import { BaseComponent } from 'src/app/shared/base.component';


@Component({
  selector: 'app-lokacija-form',
  templateUrl: './lokacija-form.component.html',
  styleUrls: ['./lokacija-form.component.css']
})
export class LokacijaFormComponent extends BaseComponent implements OnInit {

  //@Input() adresa: Adresa
  adresa: Adresa = new Adresa();
  ime:string = '';
  isForDelete = false;
  isEdit = false;
  errMsg = '';
  successMsg = '';
  lat: number;
  lng: number;
  coordinates: Coordinates[] = [];

  constructor(private _loginService: LoginService,
              private adresaService: AdresaService,
              private activatedRoute: ActivatedRoute,
              private httpClient: HttpClient,
              private router: Router) { 
    super(_loginService)
  }

  childNgOnInit() {

    console.log("Ime napisano u initu je: ", this.ime)
        
    this.adresa.koordinate = "16.33,15.44"
    //this.adresa.adresa = "Kranjčevićeva 11"
    
    // if(id) {
    //   this.isEdit = true;
    //   this.adresaService.getById(id).subscribe( 
    //     res => {
    //       this.adresa = res;
    //     }
    //   )
    // }
  }

  submit() {
    this.successMsg = '';
    this.errMsg = '';


    if(!this.validate())
        return;

    if(!this.isEdit) {
      //this.adresa.adresa = this.ime
      console.log("Nije editano znaci radi taj dio")
      console.log("adresa.adresa: ", this.adresa.adresa)
      console.log("ime: ", this.ime)
      this.adresaService.saveAdresa(this.adresa).subscribe( 
        res => {
          if(res < 0) {
            this.errMsg = 'Pogreška prilikom spremanja podataka. Pokušajte ponovo.'
            return;
          }
          this.successMsg = 'Podatci uspješno spremljeni.'
          });

    } else {
      this.adresaService.updateAdresa(this.adresa).subscribe( 
        res => {
          if(res < 0) {
            this.errMsg = 'Pogreška prilikom spremanja podataka. Pokušajte ponovo.'
            return;
          }

          this.successMsg = 'Podatci uspješno spremljeni.'
        }
      );
      }
      console.log("Adresa")
      console.log(this.adresa)
  }

  onDelete() {
    this.errMsg = '';
    this.successMsg = '';
    
    this.isForDelete = true;
  }

  onDiscardDelete() {
    this.isForDelete = false;
  }

  onConfirmDelete() {
    this.adresaService.deleteAdresa(this.adresa.id).subscribe( res => {
      if(res < 0) {
        this.errMsg = 'Pogreška prilikom brisanja podataka. Pokušajte ponovo.'
        return;
      }

      this.adresaService.deleteAdresa(this.adresa.id).subscribe( res => {
        if(res < 0) {
          this.errMsg = 'Pogreška prilikom brisanja podataka. Pokušajte ponovo.'
          return;
        }
      })
    })

    this.successMsg = 'Podatci uspješno izbrisani.'

    setTimeout( () => {
      this.router.navigateByUrl('/lokacija-form');
    }, 3000)
  }

  validate(): boolean {
    if(!this.adresa) {
      this.errMsg = 'Molimo unesite podatke.';
      return false;
    }
    return true;
  }

  async getCoordinates(adresa: Adresa){
    this.adresa = adresa;
    let params = new HttpParams()
    params = params.append("key", "AIzaSyDOQMu9XOv4rMuKcwRCGW-SqfSSPsA7Sdg");

    params = params.set("address", adresa[0]);
      this.httpClient.get("https://maps.googleapis.com/maps/api/geocode/json", {params: params})
        .subscribe((res : any) => {
            if (res.status == "OK") {
              let coordinate: Coordinates = res.results[0].geometry.location;
              this.lat = coordinate.lat;
              this.lng = coordinate.lng;
              this.coordinates.push(coordinate);
          }
        }
      )
  }
}
