import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LokacijaFormComponent } from './lokacija-form.component';

describe('LokacijaFormComponent', () => {
  let component: LokacijaFormComponent;
  let fixture: ComponentFixture<LokacijaFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LokacijaFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LokacijaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
