import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LokacijaFormComponent } from './lokacija-form.component';

const routes: Routes = [{
  path: '', component: LokacijaFormComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LokacijaFormRoutingModule { }
