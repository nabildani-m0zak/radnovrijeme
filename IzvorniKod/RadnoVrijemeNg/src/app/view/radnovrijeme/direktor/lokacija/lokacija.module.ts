import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';

import { LokacijaRoutingModule } from './lokacija-routing.module';
import { LokacijaComponent } from './lokacija.component';
import { ForbiddenPanelModule } from '../../forbidden-panel/forbidden-panel.module';
import { MapaComponent } from '../mapa/mapa.component';


@NgModule({
  declarations: [LokacijaComponent, MapaComponent],
  imports: [
    CommonModule,
    LokacijaRoutingModule,
    ForbiddenPanelModule,
    AgmCoreModule,
  ],
  exports: [LokacijaComponent, MapaComponent]
})
export class LokacijaModule { }
