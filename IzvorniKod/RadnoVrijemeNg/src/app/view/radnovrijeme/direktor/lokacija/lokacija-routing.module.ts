import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LokacijaComponent } from './lokacija.component';

const routes: Routes = [
  {
    path: '', component: LokacijaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LokacijaRoutingModule { }
