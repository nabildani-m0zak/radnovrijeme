import { HttpClient, HttpParams, JsonpClientBackend } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Adresa } from 'src/app/model/Adresa';
import { Coordinates } from 'src/app/model/Coordinates';
import { Zaposlenik } from 'src/app/model/Zaposlenik';
import { AdresaService } from 'src/app/services/adresa.service';
import { LoginService } from 'src/app/services/login.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-lokacija',
  templateUrl: './lokacija.component.html',
  styleUrls: ['./lokacija.component.css']
})
export class LokacijaComponent extends BaseComponent implements OnInit {
  private id: number
  adrese: Adresa[];
  coordinates: Coordinates[] = [];
  lat: number;
  lng: number; 

  constructor(private loginService: LoginService,
              private route: ActivatedRoute,
              private adresaService: AdresaService,
              private httpClient: HttpClient,
              private router: Router) {
    super(loginService);
  }

  childNgOnInit() {
      this.id = this.route.snapshot.params['id'];

      this.adresaService.getAllAdress(this.id).subscribe( async res => {
        if (res) {
          await this.getCoordinates(res);
        }
       }
      );
  }

  async getCoordinates(adrese: Adresa[]){
    this.adrese = adrese;
    let params = new HttpParams()
    params = params.append("key", "AIzaSyDOQMu9XOv4rMuKcwRCGW-SqfSSPsA7Sdg");
    var i = 0

    for(let adresa of adrese) {
      params = params.set("address", adresa[0]);
      this.httpClient.get("https://maps.googleapis.com/maps/api/geocode/json", {params: params})
        .subscribe((res : any) => {
            if (res.status == "OK") {
              let coordinate: Coordinates = res.results[0].geometry.location;
              this.lat = coordinate.lat;
              this.lng = coordinate.lng;
              this.coordinates.push(coordinate);
          }
        }
      )
    }
  }


   pregledZaposlenika() {
      this.router.navigateByUrl('/zaposlenici');
   }
}



