import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppUser } from 'src/app/model/AppUser';
import { LoginService } from 'src/app/services/login.service';
import { AppGlobal } from 'src/app/shared/app-global';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  /**
   * Korisničko ime upisano u formi.
   */
  username: string = '';
  /** 
   * Lozinka upisana u formi. 
  */
  password: string = '';
  /**
   * Poruka o pogrešci za prikaz na ekranu.
   */
  errMessage = '';

  constructor(private _loginService: LoginService,
             private router: Router) { }

  ngOnInit(): void {
  }

  /**
   * Provjerava ispravnost podataka unesenih u formi te šalje zahtjev s tim podacima prema poslužitelju.
   * Izvlači podatke iz generiranog tokena te ih sprema u globalnu klasu <code>AppGlobal</code>.
   */
  login() {
    if(this.validate()) {
      console.log("Username: ", this.username)
      
      this._loginService.login(this.username, this.password).subscribe(
        res => {
          if(res.token) {
            let appUser = this.extractUserFromJWTToken(res.token);
            appUser.setUsername(this.username);

            AppGlobal.setUser(appUser);
            this._loginService.onLoginSuccess();

            this.router.navigate(['/home']);
          } 
          else {
            this.errMessage = 'Pogrešno korisničko ime ili lozinka.'
          }
        }
      )
    }
  }

  /**
   * Provjerava ispravnost unesenih podataka u formi.
   * @returns istina ako su podaci ispravno uneseni, inače laž
   */
  private validate(): boolean {
    if(!this.username) {
      this.errMessage = 'Molimo unesite korisničko ime.'
      return false;
    }

    if(!(this.username.trim().length > 0)) {
      this.errMessage = 'Molimo unesite korisničko ime.'
      return false;
    }
    this.username = this.username.trim();

    if(!this.password) {
      this.errMessage = 'Molimo unesite lozinku.'
      return false;
    }
    
    if(!(this.password.trim().length > 0)) {
      this.errMessage = 'Molimo unesite lozinku.'
      return false;
    }

    this.password = this.password.trim();

    return true;
  }

  /**
   * Čita podatke o korisniku spremljene u tokenu. 
   * @param token token iz kojeg se čitaju podaci
   * @return korisnik isčitan iz tokena
   */
  private extractUserFromJWTToken(token: string): AppUser {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
  
   let userData = JSON.parse(jsonPayload);
   
   return new AppUser(userData.sub, '', userData.authorities, token);
  }

}
