import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Zaposlenik } from 'src/app/model/Zaposlenik';
import { LoginService } from 'src/app/services/login.service';
import { ZaposlenikService } from 'src/app/services/zaposlenik.service';
import { BaseComponent } from 'src/app/shared/base.component';

@Component({
  selector: 'app-zaposlenik',
  templateUrl: './zaposlenik.component.html',
  styleUrls: ['./zaposlenik.component.css']
})
export class ZaposlenikComponent extends BaseComponent implements OnInit {

  public errMsg = '';
  public succesMsg = '';

  @Input() zaposlenik: Zaposlenik;
  @Input() idDjelatnost: number;

  @Output() zaposlenikDeletedEmitter: EventEmitter<number> = new EventEmitter();
  isForDelete: boolean;

  constructor(private _loginService: LoginService,
              private _zaposlenikService: ZaposlenikService,
              private router: Router) {
    super(_loginService);
    this.isForDelete = false;
  }
  
  childNgOnInit(): void {
    this.zaposlenik.id = this.zaposlenik[0];
    this.zaposlenik.ime = this.zaposlenik[1];
    this.zaposlenik.prezime = this.zaposlenik[2];
    this.zaposlenik.radno_vrijeme_sati_poc = this.zaposlenik[3];
    this.zaposlenik.radno_vrijeme_sati_kraj = this.zaposlenik[4];
    this.zaposlenik.duljina_radnog_vremena = this.zaposlenik[5];
    this.zaposlenik.slobodni_dani = this.zaposlenik[6];
    this.zaposlenik.id_uloga = this.zaposlenik[7];
  }

  onDelete() {
    this.isForDelete = true;
  }

  onConfirmDelete() {
    this._zaposlenikService.removeZaposlenikFromGrupa(this.zaposlenik.id, this.idDjelatnost).subscribe( res => {
      if(res > 0) {
        this.zaposlenikDeletedEmitter.emit(this.zaposlenik.id);
        this.succesMsg = 'Zaposlenik uspješno obrisan!';
      }

      this.errMsg = 'Pogreška kod brisanja zaposlenika!';
    });
  }

  onDiscardDelete() {
    this.isForDelete = false;
  }
}