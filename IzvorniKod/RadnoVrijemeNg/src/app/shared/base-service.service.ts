import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppGlobal } from './app-global';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
/**
 * Osnovna service klasa koju nasljeđuju svi ostali service-i. U svakoj metodi prije poziva HttpClient-a provjerava postoji
 * li pohranjen korisnik te ako postoji šalje pohranjen token kako bi se zahtjev prema poslužitelju mogao ispravno izvršiti.
 */
export class BaseService {

  /**
   * Klasni konstruktor
   * @param httpClient 
   */
  constructor(protected httpClient: HttpClient) { }

  /**
   * Postavlja parametre http zaglavja. Ukoliko postoji pohranjen ovlašteni korisnik, u zaglavlje postavlja i token potreban
   * za ovjeru na poslužiteljskoj strani
   * @returns HttpHeaders objekt s postavljenim parametrima
   */
  private getHeaders(): HttpHeaders {
    const user = AppGlobal.getUser(null);

    if(user && user.token)
      return new HttpHeaders().set('Accept', 'application/json')
                              .set('Content-Type', 'application/json')
                              .set('Authorization', 'Bearer ' + user.token)
                              .set('Access-Control-Allow-Origin', environment.cors);

    return new HttpHeaders().set('Accept', 'application/json')
                            .set('Content-Type', 'application/json')
                            .set('Access-Control-Allow-Origin', environment.cors);
  }

  /**
   * Poziva post metodu nad HtppClient-om.
   * @param url dodatak na osnovni url na koji se šalje http zahtjev
   * @param object objekt koji se šalje u tijelu http zahtjeva
   * @returns <code>Observable</code> tijela primljenog odgovora
   */
  protected post(url: string, object: any): Observable<any> {
    const headers = this.getHeaders();
    return this.httpClient.post(environment.apiUri + url, object, {headers});
  }

  /**
   * Poziva get metodu nad HtppClient-om.
   * @param url dodatak na osnovni url na koji se šalje http zahtjev
   * @returns <code>Observable</code> tijela primljenog odgovora
   */
  protected get(url: string): Observable<any> {
    const headers = this.getHeaders();
    return this.httpClient.get(environment.apiUri + url, {headers});
  }

  /**
   * Poziva put metodu nad HtppClient-om.
   * @param url dodatak na osnovni url na koji se šalje http zahtjev
   * @param object objekt koji se šalje u tijelu http zahtjeva
   * @returns <code>Observable</code> tijela primljenog odgovora
   */
  protected put(url: string, object: any): Observable<any> {
    const headers = this.getHeaders();
    return this.httpClient.put(environment.apiUri + url, object, {headers});
  }

  /**
   * Poziva delete metodu nad HtppClient-om.
   * @param url dodatak na osnovni url na koji se šalje http zahtjev
   * @returns <code>Observable</code> tijela primljenog odgovora
   */
  protected delete(url: string): Observable<any> {
    const headers = this.getHeaders();
    return this.httpClient.delete(environment.apiUri + url, {headers});
  }
}
