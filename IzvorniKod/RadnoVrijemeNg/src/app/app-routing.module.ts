import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './view/dashboard/home/home.component';
import { GrupaZaposleniciModule } from './view/radnovrijeme/direktor/grupa-zaposlenici/grupa-zaposlenic.module';
import { LokacijaPregledModule } from './view/radnovrijeme/direktor/lokacija-pregled/lokacija-pregled.module';
import { LokacijaModule } from './view/radnovrijeme/direktor/lokacija/lokacija.module';
import { GrupaKolegeModule } from './view/radnovrijeme/djelatnik/grupa-kolege/grupa-kolege.module';
import { GrupaZaposlenikListModule } from './view/radnovrijeme/voditelj/grupa-zaposlenik-list/grupa-zaposlenik-list.module';

const routes: Routes = [
  { path: 'login', loadChildren: () => import('./view/login/login.module').then(m => m.LoginModule) },
  { path: 'upis-sati', loadChildren: () => import('./view/radnovrijeme/upis-sati/upis-sati.module')
                                    .then(m => m.UpisSatiModule)}, 

  { path: 'osobni-podatci', loadChildren: () => import('./view/radnovrijeme/osobni-podatci/osobni-podatci.module')
                                    .then(m => m.OsobniPodatciModule)},

  { path: 'izbor-voditelja/:id_djelatnost', loadChildren: () => import('./view/radnovrijeme/direktor/izbor-voditelja/izbor-voditelja.module')
                                    .then(m => m.IzborVoditeljaModule)},

  { path: 'voditelj-zadatci', loadChildren: () => import('./view/radnovrijeme/voditelj/voditelj.module')
                                    .then(m => m.VoditeljModule)},
  { path: 'grupa-zaposlenik', loadChildren: () => import('./view/radnovrijeme/voditelj/grupa-zaposlenik-list/grupa-zaposlenik-list.module')
                                    .then(m => m.GrupaZaposlenikListModule)},

  { path: 'usluzna-djelatnost', loadChildren: () => import('./view/radnovrijeme/direktor/usluzna-djelatnost-form/usluzna-djelatnost-form.module')
                                    .then(m => m.UsluznaDjelatnostFormModule)},
  { path: 'usluzna-djelatnost/:id', loadChildren: () => import('./view/radnovrijeme/direktor/usluzna-djelatnost-form/usluzna-djelatnost-form.module')
                                    .then(m => m.UsluznaDjelatnostFormModule)},
  { path: 'zaposlenici', loadChildren: () => import('./view/radnovrijeme/direktor/zaposlenici-list/zaposlenici-list.module')
                                    .then(m => m.ZaposleniciListModule)},
  { path: 'zaposlenik', loadChildren: () => import('./view/radnovrijeme/direktor/zaposlenici-form/zaposlenici-form.module')
                                    .then(m => m.ZaposleniciFormModule)},
  { path: 'zaposlenik/:id', loadChildren: () => import('./view/radnovrijeme/direktor/zaposlenici-form/zaposlenici-form.module')
                                    .then(m => m.ZaposleniciFormModule)},
  { path: 'grupe', loadChildren: () => import('./view/radnovrijeme/direktor/grupe/grupe.module')
                                    .then(m => m.GrupeModule)},
   { path: 'grupa-djelatnik', loadChildren: () => import('./view/radnovrijeme/djelatnik/grupa-djelatnik/grupa-djelatnik.module')
                                     .then(m => m.GrupaDjelatnikModule)},
  { path: 'grupa-zaposlenici/:id', loadChildren: () => import('./view/radnovrijeme/direktor/grupa-zaposlenici/grupa-zaposlenic.module')
                                    .then(m => GrupaZaposleniciModule)},
  { path: 'grupa-kolege/:id', loadChildren: () => import('./view/radnovrijeme/djelatnik/grupa-kolege/grupa-kolege.module')
                                     .then(m => GrupaKolegeModule)},
  { path: 'lokacija/:id', loadChildren: () => import('./view/radnovrijeme/direktor/lokacija/lokacija.module')
                                     .then(m => LokacijaModule)},
  { path: 'lokacija-pregled', loadChildren: () => import('./view/radnovrijeme/direktor/lokacija/lokacija.module')
                                    .then(m => LokacijaPregledModule)},
  { path: 'lokacija-form', loadChildren: () => import('./view/radnovrijeme/direktor/lokacija-form/lokacija-form.module')
                                    .then(m => m.LokacijaFormModule)},
  { path: 'lokacija-form/:id', loadChildren: () => import('./view/radnovrijeme/direktor/lokacija-form/lokacija-form.module')
                                    .then(m => m.LokacijaFormModule)},
  { path: 'home', loadChildren: () => import('./view/dashboard/dashboard.module').then(m => m.DashboardModule) },
  { path: 'dashboard', redirectTo: 'home', pathMatch: 'full' },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
