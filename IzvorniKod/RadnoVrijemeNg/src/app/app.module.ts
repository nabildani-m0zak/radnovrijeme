import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'; 

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './view/layout/footer/footer.component';
import { HeaderComponent } from './view/layout/header/header.component';
import { DropdownMenuComponent } from './view/layout/dropdown-menu/dropdown-menu.component';
import { CommonModule } from '@angular/common';
import { LoginModule } from './view/login/login.module';
import { DashboardModule } from './view/dashboard/dashboard.module';
import { BaseService } from './shared/base-service.service';
import { FormsModule } from '@angular/forms';
//import { FormControl } from '@angular/forms'
import { AgmCoreModule } from '@agm/core';
//import { LokacijaItemComponent } from './view/radnovrijeme/direktor/lokacija-item/lokacija-item.component';
//import { LokacijaFormComponent } from './view/radnovrijeme/direktor/lokacija-form/lokacija-form.component';
//import { LokacijaPregledComponent } from './view/radnovrijeme/direktor/lokacija-pregled/lokacija-pregled.component';
//import { GrupaKolegeComponent } from './view/radnovrijeme/djelatnik/grupa-kolege/grupa-kolege.component';
//import { GrupaDjelatnikComponent } from './view/radnovrijeme/djelatnik/grupa-djelatnik/grupa-djelatnik.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    DropdownMenuComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    LoginModule,
    DashboardModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDOQMu9XOv4rMuKcwRCGW-SqfSSPsA7Sdg'
    })
  ],
  providers: [BaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
