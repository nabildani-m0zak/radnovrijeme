/**
 * Sadrži podatak o tome da je status okruženja production te URI na koji se šalje zahtjev prema poslužitelju.
 */
export const environment = {
  production: true,
  apiUri: 'https://nabildanimozak.herokuapp.com/api',
  cors: 'https://nabildanim0zak.herokuapp.com'
};
