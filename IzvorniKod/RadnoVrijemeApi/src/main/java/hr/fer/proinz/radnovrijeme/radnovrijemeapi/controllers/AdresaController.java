package hr.fer.proinz.radnovrijeme.radnovrijemeapi.controllers;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IGet;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IPostUpdateDelete;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.Adresa;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.services.AdresaService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AdresaController implements IPostUpdateDelete<Adresa>, IGet<Adresa> {

    /**
     * Servis kojem controller prosljeđuje zahtjeve.
     */
    private final AdresaService adresaService;

    /**
     * Klasni konstruktor.
     *
     * @param adresaService servis kojem controller prosljeđuje zahtjeve.
     */
    public AdresaController(AdresaService adresaService) {
        this.adresaService = adresaService;
    }

    /**
     * Vraća listu svih adresa.
     * @return lista svih adresa.
     */

    @GetMapping("/adresa")
    public List<Adresa> getAll() {
        return this.adresaService.getAllAdresa();
    }

    /**
     * Vraća sve objekte adrese koji su pohranjeni u bazi s predanim id.
     * @param id jest id adrese koja se traži.
     * @return adresu.
     */

    @GetMapping("/adresa/{id}")
    public Adresa getById(@PathVariable int id){
        return this.adresaService.getAdresaById(id);
    }

    /**
     * Preko @RequestBody-a prima jednu ili više adresu te ih upisuje u tablicu
     * i vraća broj unesenih.
     * @param adresa jedna ili više adresa za unos u tablicu.
     * @return broj unesenih adresa.
     */

    @PostMapping("/adresa")
    public int save(@RequestBody Adresa adresa){
        return this.adresaService.saveAdresa(adresa);
    }

    /**
     * Preko @RequestBody-a prima jednu ili više adresu te ih mijenja u tablici
     * i vraća broj unesenih.
     * @param adresa jedna ili više adresa za promjenu u tablici.
     * @return broj promijenjenih adresa.
     */

    @PutMapping("/adresa")
    public int update(@RequestBody Adresa adresa){
        return this.adresaService.updateAdresa(adresa);
    }

    /**
     * Preko @RequestBody-a prima jednu ili više adresu te ih briše iz tablice
     * i vraća broj izbrisanih.
     * @param id jedna ili više id adresa za brisanje u tablicu.
     * @return broj izbrisanih adresa.
     */

    @DeleteMapping("/adresa/{id}")
    public int delete(@PathVariable int id){
        return this.adresaService.deleteAdresa(id);
    }

    /**
     * Vraća listu svih adresa i datuma obavljanja zadatka po id-u zaposlenika.
     * @return lista svih adresa i datuma obavljanja.
     */

    @GetMapping("/adresaZaposlenika/{id_zaposlenik}")
    public List<Adresa> getAllAdresaByZaposlenikId(@PathVariable int id_zaposlenik) {
        return this.adresaService.getAllAdresaByZaposlenikId(id_zaposlenik);
    }

}
