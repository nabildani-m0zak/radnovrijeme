package hr.fer.proinz.radnovrijeme.radnovrijemeapi.daos;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IGet;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IPostUpdateDelete;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.Adresa;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class AdresaDAO implements IGet<Adresa>, IPostUpdateDelete<Adresa> {

    /**
     * Objekt za povezivanje i izvršavanje zahvata nad bazom podataka
     * @param entityManager objekt za povezivanje i izvršavanje zahvata nad bazom podataka
     */
    private final EntityManager entityManager;

    /**
     * Konstruktor.
     */

    public AdresaDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Vraća sve objekte tipa <code>Adresa</code> koji su pohranjeni u bazi.
     * @return lista adresa, null ako baza nema pohranjenu nijednu adresu.
     */

    public List<Adresa> getAll() {
        try {
            String sql = "SELECT * FROM adrese";

            Query query = entityManager.createNativeQuery(sql);
            return (List<Adresa>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Vraća sve objekte tipa <code>Adresa</code> koji su pohranjeni u bazi s predanim id.
     * @param id jest id adrese koja se traži
     * @return vraća objekt adrese ukoliko postoji, null ako ne postoji.
     */

    public Adresa getById(int id) {
        try {
            String sql = "SELECT * FROM adrese " +
                    "WHERE id = :id";
            Query query = entityManager.createNativeQuery(sql, Adresa.class);
            query.setParameter("id", id);

            return (Adresa) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Sprema adresu u bazu podataka.
     * @param adresa koja se sprema u bazu podataka.
     * @return id stvorenu adresu u bazi podataka, 0 ako se ne uspije spremiti.
     */
    public int save(Adresa adresa) {
        try {
            String sql = "INSERT INTO adrese (koordinate, adresa) " +
                    "VALUES (:koordinate, :adresa)" +
                    "RETURNING id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("koordinate", adresa.getKoordinate());
            query.setParameter("adresa", adresa.getAdresa());

            return (int) query.getSingleResult();
        } catch (NoResultException ex) {
            return 0;
        }
    }

    /**
     * Briše objekt adrese u bazi podataka koji ima id jednak danom.
     * @param id id koji se briše iz baze podataka.
     * @return broj uspješno obrisanih adresa.
     */
    public int delete(int id) {
        try {
            String sql = "DELETE FROM adrese WHERE adrese.id = :id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", id);
            return (int) query.executeUpdate();
        } catch (NoResultException ex) {
            return 0;
        }
    }

    /**
     * Ažurira podatke o adresama na temelju id-a.
     * @param adresa kojoj se ažuriranju podaci u bazi podataka.
     * @return broj adresa u bazi podataka kojima su se ažurirali podaci.
     */
    public int update(Adresa adresa) {
        try {
            String sql = "UPDATE adrese " +
                    "SET koordinate = :koordinate, adresa = :adresa " +
                    "WHERE id = :id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", adresa.getId());
            query.setParameter("koordinate", adresa.getKoordinate());
            query.setParameter("adresa", adresa.getAdresa());


            return (int) query.executeUpdate();
        } catch (NoResultException ex) {
            return 0;
        }
    }

    /**
     * Vraća listu svih adresa i datuma obavljanja zadatka po id-u zaposlenika.
     * @return lista svih adresa i datuma obavljanja.
     */

    public List<Adresa> getAllAdresaByZaposlenikId(int id_zaposlenik) {
        try {
            String sql = "Select adrese.adresa, teren.datum, teren.id_zaposlenik " +
                    "From adrese " +
                    "JOIN teren ON teren.id_adresa = adrese.id " +
                    "WHERE id_zaposlenik = :id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", id_zaposlenik);
            return (List<Adresa>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }
}
