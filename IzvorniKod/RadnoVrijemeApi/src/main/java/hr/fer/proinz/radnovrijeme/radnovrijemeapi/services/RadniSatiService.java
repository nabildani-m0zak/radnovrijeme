package hr.fer.proinz.radnovrijeme.radnovrijemeapi.services;


import hr.fer.proinz.radnovrijeme.radnovrijemeapi.daos.RadniSatiDAO;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.RadniSati;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RadniSatiService {

    /**
     * Objekt DAO adrese.
     */
    private final RadniSatiDAO radniSatiDAO;

    /**
     * Klasni konstruktor.
     * @param radniSatiDAO objekt DAO adrese
     */

    public RadniSatiService(RadniSatiDAO radniSatiDAO) {
        this.radniSatiDAO = radniSatiDAO;
    }

    /**
     * Vraća sve objekte tipa <code>Radni sati</code> koji su pohranjeni u bazi.
     * @return lista radnih sati, null ako baza nema pohranjenu nijedan radni sat.
     */

    public List<RadniSati> getAllRadniSati() {return this.radniSatiDAO.getAll();}

    /**
     * Sprema radne sate u bazu podataka.
     * @param  radniSati se sprema u bazu podataka.
     * @return 1 ukoliko su radni sati uneseni, 0 ako nisu.
     */

    public int saveRadniSati(RadniSati radniSati) {return this.radniSatiDAO.save(radniSati);}

    /**
     * Dohvat stvarne realizacije pojedinog zaposlenika
     * @param  id označava id zaposelnika.
     * @return broj utrošenih sati, inače 0.
     */

    public int getStvarnaRealizacijaZaposelnika(int id) {
        return this.radniSatiDAO.getStvarnaRealizacijaZaposelnika(id);
    }

    /**
     * Dohvat materijalne realizacije pojedinog zaposlenika
     * @param  id označava id zaposelnika.
     * @return broj utrošenih sati, inače 0.
     */

    public int getMaterijalnaRealizacijaZaposelnika(int id) {
        return this.radniSatiDAO.getMaterijalnaRealizacijaZaposelnika(id);
    }
}
