package hr.fer.proinz.radnovrijeme.radnovrijemeapi.daos;


import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IGet;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IPostUpdateDelete;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.RadniSati;


import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;

@Repository
@Transactional
public class RadniSatiDAO implements IGet<RadniSati>, IPostUpdateDelete<RadniSati> {

    /**
     * Objekt za povezivanje i izvršavanje zahvata nad bazom podataka
     * @param entityManager objekt za povezivanje i izvršavanje zahvata nad bazom podataka
     */
    private final EntityManager entityManager;

    /**
     * Konstruktor.
     */

    public RadniSatiDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Vraća sve objekte tipa <code>Radni sati</code> koji su pohranjeni u bazi.
     * @return lista adresa, null ako baza nema pohranjenu nijednu adresu.
     */

    public List<RadniSati> getAll() {
        try {
            String sql = "SELECT * FROM radni_sati";

            Query query = entityManager.createNativeQuery(sql);
            return (List<RadniSati>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public RadniSati getById(int id) {
        return null;
    }

    /**
     * Sprema radne sate u bazu podataka.
     * @param radniSati koja se sprema u bazu podataka.
     * @return 1 ako je uspjesno unesao , 0 ako se ne uspije unesti.
     */
    public int save(RadniSati radniSati) {
        try {
            String sql = "INSERT INTO radni_sati (id_zaposlenik, id_radni_zadatak, duljina_rada, datum)" +
                    "VALUES (:id_zaposlenik, :id_radni_zadatak, :duljina_rada, CURRENT_DATE)";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id_zaposlenik", radniSati.getId_zaposlenik());
            query.setParameter("id_radni_zadatak", radniSati.getId_radni_zadatak());
            query.setParameter("duljina_rada", radniSati.getDuljina_rada());

            return 1;
        } catch (NoResultException ex) {
            return 0;
        }
    }

    @Override
    public int update(RadniSati radniSati) {
        return 0;
    }

    @Override
    public int delete(int id) {
        return 0;
    }

    /**
     * Dohvat stvarne realizacije pojedinog zaposlenika
     * @param  id označava id zaposelnika.
     * @return broj utrošenih sati, inače 0.
     */

    public int getStvarnaRealizacijaZaposelnika(int id) {
        try {
            String sql = "SELECT radni_sati.duljina_rada " +
            "FROM radni_sati " +
            "WHERE id_zaposlenik = :id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", id);
            return query.getSingleResult() == null ? 0 :  (int) query.getSingleResult();
        } catch (NoResultException ex) {
            return 0;
        }
    }

    /**
     * Dohvat materijalne realizacije pojedinog zaposlenika
     * @param  id označava id zaposelnika.
     * @return broj utrošenih sati, inače 0.
     */

    public int getMaterijalnaRealizacijaZaposelnika(int id) {
        try {
            String sql = "SELECT radni_zadatci.cijena_sata * radni_sati.duljina_rada " +
            "FROM radni_zadatci " +
            "JOIN radni_sati ON radni_zadatci.id = radni_sati.id_radni_zadatak " +
            "WHERE radni_zadatci.id_zaposlenik = :id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", id);
            return query.getSingleResult() == null ? 0 :  (int) query.getSingleResult();
        } catch (NoResultException ex) {
            return 0;
        }
    }
}
