package hr.fer.proinz.radnovrijeme.radnovrijemeapi.controllers;


import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IGet;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IPostUpdateDelete;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.Adresa;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.RadniSati;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.services.RadniSatiService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RadniSatiController implements IPostUpdateDelete<RadniSati>{

    /**
     * Servis kojem controller prosljeđuje zahtjeve.
     */
    private final RadniSatiService radniSatiService;

    /**
     * Klasni konstruktor.
     * @param radniSatiService servis kojem controller prosljeđuje zahtjeve.
     */
    public RadniSatiController(RadniSatiService radniSatiService) {
        this.radniSatiService = radniSatiService;
    }

    /**
     * Preko @RequestBody-a prima jednu ili više adresu te ih upisuje u tablicu
     * i vraća broj unesenih.
     * @param radniSati jedna ili više adresa za unos u tablicu.
     * @return broj unesenih adresa.
     */

    @PostMapping("/radnisati")
    public int save(@RequestBody RadniSati radniSati){
        return this.radniSatiService.saveRadniSati(radniSati);
    }

    @GetMapping("/radnisati/stvarnaRealizacija/{id}")
    public int getStvarnaRealizacijaZaposelnika(@PathVariable int id){
        return this.radniSatiService.getStvarnaRealizacijaZaposelnika(id);
    }

    @GetMapping("/radnisati/materijalnaRealizacija/{id}")
    public int getMaterijalnaRealizacijaZaposelnika(@PathVariable int id){
        return this.radniSatiService.getMaterijalnaRealizacijaZaposelnika(id);
    }

    @Override
    public int update(RadniSati radniSati) {
        return 0;
    }

    @Override
    public int delete(int id) {
        return 0;
    }


}
