package hr.fer.proinz.radnovrijeme.radnovrijemeapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "zaposlenik_grupa")
@Entity
public class Zaposlenik_grupa {
    @Id
    int id_zaposlenik;
    @Column(nullable = false)
    int id_grupa;

    public int getId_zaposlenik() {
        return id_zaposlenik;
    }

    public int getId_grupa() {
        return id_grupa;
    }

    @Override
    public String toString() {
        return "Id zap: " + id_zaposlenik + ", Id grup: " + id_grupa;
    }
}
