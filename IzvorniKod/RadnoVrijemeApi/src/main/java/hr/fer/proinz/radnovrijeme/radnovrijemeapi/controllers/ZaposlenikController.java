package hr.fer.proinz.radnovrijeme.radnovrijemeapi.controllers;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IGet;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IPostUpdateDelete;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.Zaposlenik;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.services.ZaposlenikService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ZaposlenikController implements IGet<Zaposlenik>, IPostUpdateDelete<Zaposlenik> {

    private final ZaposlenikService zaposlenikService;


    public ZaposlenikController(ZaposlenikService zaposlenikService) {
        this.zaposlenikService = zaposlenikService;
    }

    /**
     * Vraća listu svih zaposlenika
     *
     * @return lista svih zaposlenika
     */
    @GetMapping("/zaposlenik")
    public List<Zaposlenik> getAll() {
        return this.zaposlenikService.getAllZaposlenik();
    }

    /**
     * Vraća listu svih voditelja
     *
     * @return lista svih voditelja
     */
    @GetMapping("/voditelj")
    public List<Zaposlenik> getAllVoditelj() {
        return this.zaposlenikService.getAllVoditelj();
    }

    /**
     * Vraća voditelja kojem je pridijeljena uslužna djelatnost s odgovorajućim id-om.
     *
     * @param id jedinstveni identifikator djelatnosti
     * @return lista svih voditelja
     */
    @GetMapping("/voditelj/djelatnost/{id}")
    public Zaposlenik getVoditeljByDjelatnostId(@PathVariable int id) {
        return this.zaposlenikService.getVoditeljByDjelatnostId(id);
    }

    /**
     * Vraća djelatnika kojem je pridijeljen radni zadatak s odgovorajućim id-om.
     *
     * @param id jedinstveni identifikator radnog zadatka
     * @return lista svih voditelja
     */
    @GetMapping("/djelatnik/radnizadatak/{id}")
    public Zaposlenik getDjelatnikByZadatakId(@PathVariable int id) {
        return this.zaposlenikService.getDjelatnikByZadatakId(id);
    }

    /**
     * Vraća listu svih djelatnika
     *
     * @return lista svih djelatnika
     */
    @GetMapping("/djelatnik")
    public List<Zaposlenik> getAllDjelatnik() {
        return this.zaposlenikService.getAllDjelatnik();
    }

    /**
     * Vraća zaposlenika prema zadanom jedinstvenom identifikatoru.
     *
     * @param id jedinstveni identifikator zaposlenika.
     * @return zaposlenik
     */
    @GetMapping("/zaposlenik/{id}")
    public Zaposlenik getById(@PathVariable int id) {
        return this.zaposlenikService.getZaposlenikById(id);
    }

    /**
     * Preko @RequestBody-a prima jednog ili vise zaposlenika te ih upisuje u tablicu
     * i vraća broj unesenih.
     *
     * @param zaposlenik jednan ili više zaposlenika za unos u tablicu.
     * @return broj unesenih zaposlenika
     */
    @PostMapping("/zaposlenik")
    public int save(@RequestBody Zaposlenik zaposlenik) {
        return this.zaposlenikService.saveZaposlenik(zaposlenik);
    }

    /**
     * Preko @RequestBody-a prima jednog ili vise zaposlenika te ih upisuje u tablicu
     * i vraća broj unesenih.
     *
     * @param zaposlenik jedan ili višezaposlenika za promjenu u tablici.
     * @return broj promjenjenih zaposlenika
     */
    @PutMapping("/zaposlenik")
    public int update(@RequestBody Zaposlenik zaposlenik) {
        return this.zaposlenikService.updateZaposlenik(zaposlenik);
    }

    /**
     * Prima jedan id zaposlenika te ga briše u tablici
     * i vraća broj izbrisanih.
     *
     * @param id jedan ili više id-eva zaposlenika za brisanje u tablici.
     * @return broj izbrisanih zaposlenika
     */
    @DeleteMapping("/zaposlenik/{id}")
    public int delete(@PathVariable int id) {
        return this.zaposlenikService.deleteZaposlenik(id);
    }

    /**
     * Briše objekt zaposlenik_grupa u bazi podataka koji ima jedak id_zaposlenika zadanom <code>id_zoposlenik</code> i
     * koji se nalazi u grupi od voditelja koji ima id jednak <code>id_voditelj</code>
     *
     * @param id_zaposlenik id zaposlenika čije objetke zaposlenik_grupa treba izbrisati
     * @param id_voditelj   id voditelja u čijoj grupi mora biti objekt zaposlenik_grupa izbrisan
     * @return broj uspješno obrisanih zaposlenik_grupa.
     */
    @DeleteMapping("/zaposlenik/{id_zaposlenik}/voditelj/{id_voditelj}")
    public int deleteZaposlenikFromGrupa(@PathVariable int id_zaposlenik, @PathVariable int id_voditelj) {
        return this.zaposlenikService.deleteZaposlenikFromGrupa(id_zaposlenik, id_voditelj);
    }

    /**
     * Dohvaća sve podatke o zaposlenicima <code>zaposlenik</code> čiji voditelj ima dani id.
     *
     * @param id id voditelja svih zaposelnika.
     * @return lista zaposlenika u bazi podataka kojima je voditelj s danim id-om.
     */

    @GetMapping("/zaposlenik/voditelj/{id}")
    public List<Zaposlenik> getZaposlenikWithIdVoditelj(@PathVariable int id) {
        return this.zaposlenikService.getZaposlenikWithIdVoditelj(id);
    }

    /**
     * Pridoda zaposlenika sa idZaposlenik grupi kojoj voditelj ima id idVoditeljGrupe
     *
     * @param idVoditeljGrupe id voditelja grupe kojoj pridodajemo zaposlenika
     * @param idZaposlenik    id zaposlenika kojeg pridodajemo grupi
     * @return broj izmjenjenih redaka
     */
    @PostMapping("/zaposlenik/{idZaposlenik}/voditelj/{idVoditeljGrupe}")
    public int pridjeliZaposlenikaGrupi(@PathVariable int idVoditeljGrupe, @PathVariable int idZaposlenik) {
        return this.zaposlenikService.pridjeliZaposlenikaGrupi(idVoditeljGrupe, idZaposlenik);
    }

    /**
     * Vraća objekt zaposlenika preko zadanog id-ja korisnika.
     *
     * @param id id korisnika preko kojeg se dobiva objekt zaposlenika
     * @return objekt zaposlenika.
     */
    @GetMapping("/zaposlenik/korisnik/{id}")
    public Zaposlenik getZaposlenikByKorisnikId(@PathVariable int id) {
        return zaposlenikService.getZaposlenikByKorisnikId(id);
    }

    /**
     * Vraca listu listi zaposlenika u kojem je svaka unutarnja lista jedna grupa
     *
     * @return Lista listi zaposlenika
     */
    @GetMapping("/zaposlenik/bygroup")
    public List<List<Zaposlenik>> dohvatiListuListiZaposlenika() {
        return this.zaposlenikService.dohvatiListuListiZaposlenika();
    }

    /**
     * Vraca listu listi zaposlenika u kojem je svaka unutarnja lista jedna grupa kojoj zaposlenik za idZaposlenika pripada
     *
     * @return Lista listi zaposlenika
     */
    @GetMapping("/zaposlenik/bygroup/{id}")
    public List<List<Zaposlenik>> dohvatiGrupeUKojimaJeZaposlenik(@PathVariable int id) {
        return zaposlenikService.dohvatiGrupeUKojimaJeZaposlenik(id);
    }

    /**
     * Vraca zauzetost radnika u satima
     *
     * @param id id radnika
     * @return int koji predstavlja sate zauzeca
     */
    @GetMapping("/zaposlenik/zauzetost/{id}")
    public int getZauzetostDjelatnik(@PathVariable int id) {
        return zaposlenikService.getZauzetostDjelatnik(id);
    }
}
