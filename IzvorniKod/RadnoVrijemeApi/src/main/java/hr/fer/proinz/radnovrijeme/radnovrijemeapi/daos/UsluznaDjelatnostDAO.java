package hr.fer.proinz.radnovrijeme.radnovrijemeapi.daos;


import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IGet;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IPostUpdateDelete;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.UsluznaDjelatnost;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;

/**
 * Radi operacije s tablicom uslužne djelatnosti nad bazom.
 */
@Repository
@Transactional
public class UsluznaDjelatnostDAO implements IGet<UsluznaDjelatnost>, IPostUpdateDelete<UsluznaDjelatnost> {

    /**
     * Objekt za povezivanje i izvršavanje zahvata nad bazom podataka.
     */
    private final EntityManager entityManager;

    /**
     * Klasni konstruktor.
     *
     * @param entityManager objekt za povezivanje i izvršavanje zahvata nad bazom podataka
     */
    public UsluznaDjelatnostDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Vraća sve objekte tipa <code>UsluznaDjelatnost</code> koji su pohranjeni u bazi.
     *
     * @return lista uslužnih djelatnosti, null ako baza nema pohranjeno nijednu uslužnu djelatnost.
     */
    public List<UsluznaDjelatnost> getAll() {
        try {
            String sql = "SELECT * FROM usluzne_djelatnosti";

            Query query = entityManager.createNativeQuery(sql);
            return (List<UsluznaDjelatnost>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Vraca uslužnu djelatnost iz baze podataka prema id-u <code>id</code>.
     *
     * @param id id prema kojem se traži uslužna djelatnost
     * @return objekt uslužne djelatnosti koji se pronađe, null ako ne postoji takva uslužna djelatnost.
     */
    public UsluznaDjelatnost getById(int id) {
        try {
            String sql = "SELECT * FROM usluzne_djelatnosti " +
                    "WHERE id = :id";
            Query query = entityManager.createNativeQuery(sql, UsluznaDjelatnost.class);
            query.setParameter("id", id);

            return (UsluznaDjelatnost) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Vraca uslužnu djelatnost iz baze podataka prema id-u <code>id</code> voditelja.
     *
     * @param id id voditelja prema kojem se traži uslužna djelatnost
     * @return objekt uslužne djelatnosti koji se pronađe, null ako ne postoji takva uslužna djelatnost.
     */
    public UsluznaDjelatnost getByIdVoditelj(int id) {
        try {
            String sql = "SELECT usluzne_djelatnosti.* FROM usluzne_djelatnosti " +
                    "JOIN grupe ON grupe.id_djelatnost = usluzne_djelatnosti.id " +
                    "WHERE id_voditelj = :id";
            Query query = entityManager.createNativeQuery(sql, UsluznaDjelatnost.class);
            query.setParameter("id", id);

            return (UsluznaDjelatnost) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public int getGrupaByDjelatnostId(int id) {
        try {
            String sql = "SELECT id FROM grupe " +
                    "WHERE id_djelatnost = :id";
            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", id);

            return (int) query.getSingleResult();
        } catch (NoResultException e) {
            return 0;
        }
    }
    /**
     * Sprema uslužnu djelatnost <code>usluzna djelatnost</code> u bazu podataka.
     *
     * @param usluznaDjelatnost <code>usluznaDjelatnost</code> koja se sprema u bazu podataka.
     * @return id stvorene uslužne djelatnosti u bazi podataka, 0 ako se ne uspije spremiti.
     */
    public int save(UsluznaDjelatnost usluznaDjelatnost) {
        try {
            String sql = "INSERT INTO usluzne_djelatnosti (opis, naziv, cijena_sata) " +
                    "VALUES (:opis, :naziv, :cijena_sata) " +
                    "RETURNING id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("opis", usluznaDjelatnost.getOpis());
            query.setParameter("naziv", usluznaDjelatnost.getNaziv());
            query.setParameter("cijena_sata", usluznaDjelatnost.getCijenaSata());

            return (int) query.getSingleResult();
        } catch (NoResultException ex) {
            return 0;
        }
    }

    /**
     * Sprema grupu <code>grupa</code> u bazu podataka.
     *
     * @param usluznaDjelatnost <code>usluznaDjelatnost</code> koja se sprema u bazu podataka.
     * @param id_voditelj                jedinstveni identifikator uslužne djelatnosti
     * @return id stvorene grupe u bazi podataka, 0 ako se ne uspije spremiti.
     */
    public int saveGrupa(UsluznaDjelatnost usluznaDjelatnost, int id_voditelj) {
        try {
            String sql = "INSERT INTO grupe (naziv, id_djelatnost, id_voditelj) " +
                    "VALUES (:naziv, :id_djelatnost, :id_voditelj)";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("naziv", usluznaDjelatnost.getNaziv());
            query.setParameter("id_djelatnost", usluznaDjelatnost.getId());
            query.setParameter("id_voditelj", id_voditelj);

            return query.executeUpdate();
        } catch (NoResultException ex) {
            return 0;
        }
    }

    /**
     * Briše objekt uslužne djelatnosti u bazi podataka koji ima id jednak danom <code>id</code>-u.
     *
     * @param id id uslužne djelatnosti koji se briše iz baze podataka.
     * @return broj uspješno obrisanih uslužnih djelatnosti.
     */
    public int delete(int id) {
        try {
            String sql = "DELETE FROM usluzne_djelatnosti WHERE usluzne_djelatnosti.id = :id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", id);
            return (int) query.executeUpdate();
        } catch (NoResultException ex) {
            return -1;
        }
    }
    /**
     * Briše objekt zaposlenik_grupa u bazi podataka koji ima id djelatnosti jednak danom <code>id</code>-u.
     *
     * @param id id uslužne djelatnosti zaposlenika grupe koji se briše iz baze podataka.
     * @return broj uspješno obrisanih zaposlenika_grupa.
     */
    public int deleteZaposlenikGrupaByIdDjelatnost(int id) {
        try {
            String sql = "DELETE FROM zaposlenik_grupa WHERE id_grupa = (SELECT id FROM grupe WHERE grupe.id_djelatnost = :id) ; " ;

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", id);
            return (int) query.executeUpdate();
        } catch (NoResultException ex) {
            return -1;
        }
    }

    /**
     * Briše objekt grupa u bazi podataka koji ima id djelatnosti jednak danom <code>id</code>-u.
     *
     * @param id id uslužne djelatnosti grupe koji se briše iz baze podataka.
     * @return broj uspješno obrisanih grupa.
     */
    public int deleteGrupaByIdDjelatnost(int id) {
        try {
            String sql = "DELETE FROM grupe WHERE grupe.id_djelatnost = :id ; " ;

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", id);
            return (int) query.executeUpdate();
        } catch (NoResultException ex) {
            return -1;
        }
    }

    /**
     * Briše objekt radnih sati u bazi podataka koji ima id jednak zadanom <code>id</code>-u.
     *
     * @param id_djelatnost id djelatnosti radnih sati koji se briše iz baze podataka.
     * @return broj uspješno obrisanih radnih sati.
     */

    public int deleteRadniSatiByIdDjelatnost(int id_djelatnost) {
        try {
            String sql = "DELETE FROM radni_sati " +
                    "WHERE id_radni_zadatak " +
                    "IN (SELECT id FROM radni_zadatci WHERE radni_zadatci.id_djelatnost = :id_djelatnost) ; ";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id_djelatnost", id_djelatnost);

            return (int) query.executeUpdate();
        } catch (NoResultException e) {
            return 0;
        }
    }

    /**
     * Ažurira podatke o uslužnim djelatnostima <code>Usluznadjelatnost</code> na temelju id-a.
     *
     * @param usluznaDjelatnost uslužna djelatnost kojoj se ažuriranju podaci u bazi podataka.
     * @return broj uslužne djelatnosti u bazi podataka kojima su se ažurirali podaci.
     */
    public int update(UsluznaDjelatnost usluznaDjelatnost) {
        try {
            String sql = "UPDATE usluzne_djelatnosti " +
                    "SET opis = :opis, naziv = :naziv, cijena_sata = :cijena_sata " +
                    "WHERE id = :id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", usluznaDjelatnost.getId());
            query.setParameter("opis", usluznaDjelatnost.getOpis());
            query.setParameter("naziv", usluznaDjelatnost.getNaziv());
            query.setParameter("cijena_sata", usluznaDjelatnost.getCijenaSata());

            return (int) query.executeUpdate();
        } catch (NoResultException ex) {
            return 0;
        }
    }

    /**
     * Prima id djelatnosti i voditelja te u tablici grupe gdje je id djelatnosti jednak poslanome
     * mijenja id voditelja na dani id
     *
     * @param id_djelatnost id te djelatnosti
     * @param id_voditelj   id tog voditelja
     * @return broj promijenjenih redaka
     */

    public int pridjeliVoditeljaDjelatnosti(int id_djelatnost, int id_voditelj) {
        try {
            String sql = "UPDATE grupe " +
                    "SET id_voditelj = :id_voditelj " +
                    "WHERE id_djelatnost = :id_djelatnost";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id_voditelj", id_voditelj);
            query.setParameter("id_djelatnost", id_djelatnost);

            return (int) query.executeUpdate();
        } catch (NoResultException ex) {
            return 0;
        }
    }

    /**
     * Prima id voditelja grupe <code>id_voditelj</code> i vraća planirani trošak grupe koju on vodi
     * @param id_voditelj <code>id</code> voditelja grupe za koju nas zanima trošak
     * @return planirani trošak grupe
     */
    public BigInteger getPlaniraniTrosakGrupe(int id_voditelj) {
        try {
            String sql = "SELECT SUM(radni_zadatci.procijenjeni_trosak)" +
                    "    FROM grupe\n" +
                    "    JOIN usluzne_djelatnosti\n" +
                    "        ON grupe.id_djelatnost = usluzne_djelatnosti.id\n" +
                    "    JOIN radni_zadatci\n" +
                    "        ON grupe.id_djelatnost = radni_zadatci.id_djelatnost\n" +
                    "    WHERE grupe.id_voditelj = :id_voditelj";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id_voditelj", id_voditelj);

            return query.getSingleResult() == null ? BigInteger.valueOf(0) : (BigInteger) query.getSingleResult();
        } catch (NoResultException ex) {
            return BigInteger.valueOf(0);
        }
    }

    /**
     * Prima id voditelja grupe <code>id_voditelj</code> i vraća realizirani trošak grupe koju on vodi
     * @param id_voditelj <code>id</code> voditelja grupe za koju nas zanima trošak
     * @return realizirani trošak grupe
     */
    public BigInteger getRealiziraniTrosakGrupe(int id_voditelj) {
        try {
            String sql = "SELECT SUM(CASE WHEN radni_zadatci.cijena_sata IS NOT NULL\n" +
                    "                THEN radni_zadatci.cijena_sata * radni_sati.duljina_rada\n" +
                    "            ELSE usluzne_djelatnosti.cijena_sata * radni_sati.duljina_rada\n" +
                    "       END) AS stupac\n" +
                    "    FROM grupe\n" +
                    "    JOIN usluzne_djelatnosti\n" +
                    "        ON grupe.id_djelatnost = usluzne_djelatnosti.id\n" +
                    "    JOIN radni_zadatci\n" +
                    "        ON grupe.id_djelatnost = radni_zadatci.id_djelatnost\n" +
                    "    JOIN radni_sati\n" +
                    "        ON radni_zadatci.id = radni_sati.id_radni_zadatak\n" +
                    "    WHERE grupe.id_voditelj = :id_voditelj";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id_voditelj", id_voditelj);

            return query.getSingleResult() == null ? BigInteger.valueOf(0) : (BigInteger) query.getSingleResult();
        } catch (NoResultException ex) {
            return BigInteger.valueOf(0);
        }
    }
}
