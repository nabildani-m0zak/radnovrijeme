package hr.fer.proinz.radnovrijeme.radnovrijemeapi.services;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.daos.UsluznaDjelatnostDAO;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.UsluznaDjelatnost;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

/**
 * Poziva ispravan DAO objekt te ispravnu metodu nad njim kako bi se izvršila zatražena akcija.
 */
@Service
public class UsluznaDjelatnostService {

    /**
     * Objekt DAO uslužne djelatnosti.
     */
    private final UsluznaDjelatnostDAO usluznaDjelatnostDAO;

    /**
     * Klasni konstruktor.
     *
     * @param usluznaDjelatnostDAO objekt DAO usluzne djelatnosti
     */
    public UsluznaDjelatnostService(UsluznaDjelatnostDAO usluznaDjelatnostDAO) {
        this.usluznaDjelatnostDAO = usluznaDjelatnostDAO;
    }

    /**
     * Vraća sve objekte tipa <code>UsluznaDjelatnost</code> koji su pohranjeni u bazi.
     *
     * @return lista uslužnih djelatnosti, null ako baza nema pohranjeno nijednu uslužnu djelatnost.
     */
    public List<UsluznaDjelatnost> getAllUsluznaDjelatnost() {
        return this.usluznaDjelatnostDAO.getAll();
    }

    /**
     * Vraca uslužnu djelatnost iz baze podataka prema id-u <code>id</code>.
     *
     * @param id id prema kojem se traži uslužna djelatnost
     * @return objekt uslužne djelatnosti koji se pronađe, null ako ne postoji takva uslužna djelatnost.
     */
    public UsluznaDjelatnost getUsluznaDjelatnostById(int id) {
        return this.usluznaDjelatnostDAO.getById(id);
    }

    /**
     * Vraca uslužnu djelatnost iz baze podataka prema id-u <code>id</code> voditelja.
     *
     * @param id id prema kojem se traži uslužna djelatnost
     * @return objekt uslužne djelatnosti koji se pronađe, null ako ne postoji takva uslužna djelatnost.
     */
    public UsluznaDjelatnost getUsluznaDjelatnostByIdVoditelj(int id) {
        return this.usluznaDjelatnostDAO.getByIdVoditelj(id);
    }

    /**
     * Sprema uslužnu djelatnost <code>usluzna djelatnost</code> u bazu podataka.
     *
     * @param usluznaDjelatnost <code>usluznaDjelatnost</code> koja se sprema u bazu podataka.
     * @return id stvorene uslužne djelatnosti u bazi podataka, 0 ako se ne uspije spremiti.
     */
    public int saveUsluznaDjelatnost(UsluznaDjelatnost usluznaDjelatnost) {
        return this.usluznaDjelatnostDAO.save(usluznaDjelatnost);
    }


    /**
     * Sprema grupu <code>grupa</code> u bazu podataka.
     *
     * @param usluznaDjelatnost <code>usluznaDjelatnost</code> koja se sprema u bazu podataka.
     * @param id                jedinstveni identifikator grupe koja se sprema
     * @return id stvorene grupe u bazi podataka, 0 ako se ne uspije spremiti.
     */
    public int saveGrupa(UsluznaDjelatnost usluznaDjelatnost, int id) {
        return this.usluznaDjelatnostDAO.saveGrupa(usluznaDjelatnost, id);
    }

    /**
     * Briše objekt uslužne djelatnosti u bazi podataka koji ima id jednak danom <code>id</code>-u.
     *
     * @param id id uslužne djelatnosti koji se briše iz baze podataka.
     * @return broj uspješno obrisanih uslužnih djelatnosti.
     */
    public int deleteUsluznaDjelatnost(int id) {
        this.usluznaDjelatnostDAO.deleteRadniSatiByIdDjelatnost(id);
        this.usluznaDjelatnostDAO.deleteZaposlenikGrupaByIdDjelatnost(id);
        this.usluznaDjelatnostDAO.deleteGrupaByIdDjelatnost(id);
        return this.usluznaDjelatnostDAO.delete(id);
    }

    /**
     * Ažurira podatke o uslužnim djelatnostima <code>Usluznadjelatnost</code> na temelju id-a.
     *
     * @param usluznaDjelatnost uslužna djelatnost kojoj se ažuriranju podaci u bazi podataka.
     * @return broj uslužnih djelatnosti u bazi podataka kojima su se ažurirali podaci.
     */
    public int updateUsluznaDjelatnost(UsluznaDjelatnost usluznaDjelatnost) {
        return this.usluznaDjelatnostDAO.update(usluznaDjelatnost);
    }

    /**
     * Prima id djelatnosti i voditelja te u tablici grupe gdje je id djelatnosti jednak poslanome
     * mijenja id voditelja na dani id
     *
     * @param id_djelatnost id te djelatnosti
     * @param id_voditelj   id tog voditelja
     * @return broj promijenjenih redaka
     */
    public int pridjeliVoditeljaDjelatnosti(int id_djelatnost, int id_voditelj) {
        UsluznaDjelatnost djelatnost = this.usluznaDjelatnostDAO.getById(id_djelatnost);
        int id_grupa = this.usluznaDjelatnostDAO.getGrupaByDjelatnostId(id_djelatnost);

        if(id_grupa == 0) {
            return this.usluznaDjelatnostDAO.saveGrupa(djelatnost, id_voditelj);
        }
        return this.usluznaDjelatnostDAO.pridjeliVoditeljaDjelatnosti(id_djelatnost, id_voditelj);
    }

    /**
     * Prima id voditelja grupe <code>id_voditelj</code> i vraća planirani trošak grupe koju on vodi
     * @param id_voditelj <code>id</code> voditelja grupe za koju nas zanima trošak
     * @return planirani trošak grupe
     */
    public BigInteger getPlaniraniTrosakGrupe(int id_voditelj) {
        return this.usluznaDjelatnostDAO.getPlaniraniTrosakGrupe(id_voditelj);
    }

    /**
     * Prima id voditelja grupe <code>id_voditelj</code> i vraća realizirani trošak grupe koju on vodi
     * @param id_voditelj <code>id</code> voditelja grupe za koju nas zanima trošak
     * @return realizirani trošak grupe
     */
    public BigInteger getRealiziraniTrosakGrupe(int id_voditelj) {
        return this.usluznaDjelatnostDAO.getRealiziraniTrosakGrupe(id_voditelj);
    }

}
