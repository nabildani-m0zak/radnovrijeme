package hr.fer.proinz.radnovrijeme.radnovrijemeapi.services;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.daos.AdresaDAO;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.Adresa;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdresaService {

    /**
     * Objekt DAO adrese.
     */
    private final AdresaDAO adresaDAO;

    /**
     * Klasni konstruktor.
     * @param adresaDAO objekt DAO adrese
     */

    public AdresaService(AdresaDAO adresaDAO) {
        this.adresaDAO = adresaDAO;
    }

    /**
     * Vraća sve objekte  Adrese koji su pohranjeni u bazi.
     * @return lista adresa, null ako baza nema pohranjeno nijednu adresu.
     */

    public List<Adresa> getAllAdresa() {
        return this.adresaDAO.getAll();
    }

    /**
     * Vraća sve objekte tipa <code>Adresa</code> koji su pohranjeni u bazi s predanim id.
     * @param id jest id adrese koja se traži
     * @return vraća objekt adrese ukoliko postoji, null ako ne postoji.
     */

    public Adresa getAdresaById(int id){
        return this.adresaDAO.getById(id);
    }

    /**
     * Sprema adresu u bazu podataka.
     * @param adresa koja se sprema u bazu podataka.
     * @return id stvorenu adresu u bazi podataka, 0 ako se ne uspije spremiti.
     */

    public int saveAdresa(Adresa adresa){
        return this.adresaDAO.save(adresa);
    }

    /**
     * Briše objekt adrese u bazi podataka koji ima id jednak danom.
     * @param id id koji se briše iz baze podataka.
     * @return broj uspješno obrisanih adresa.
     */

    public int deleteAdresa(int id){
        return this.adresaDAO.delete(id);
    }

    /**
     * Ažurira podatke o adresama na temelju id-a.
     * @param adresa kojoj se ažuriranju podaci u bazi podataka.
     * @return broj adresa u bazi podataka kojima su se ažurirali podaci.
     */

    public int updateAdresa(Adresa adresa){
        return this.adresaDAO.update(adresa);
    }

    /**
     * Vraća listu svih adresa i datuma obavljanja zadatka po id-u zaposlenika.
     * @return lista svih adresa i datuma obavljanja.
     */

    public List<Adresa> getAllAdresaByZaposlenikId(int id_zaposlenik) {
        return this.adresaDAO.getAllAdresaByZaposlenikId(id_zaposlenik);
    }
}
