package hr.fer.proinz.radnovrijeme.radnovrijemeapi.daos;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IGet;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IPostUpdateDelete;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.RadniZadatak;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.UsluznaDjelatnost;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.Zaposlenik;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.Zaposlenik_grupa;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Radi operacije s tablicom zaposlenici nad bazom
 */
@Repository
@Transactional
public class ZaposlenikDAO implements IGet<Zaposlenik>, IPostUpdateDelete<Zaposlenik> {

    /**
     * Objekt za povezivanje i izvrsavanje zahtvata nad bazom podataka.
     */
    private final EntityManager entityManager;

    /**
     * Klasni konstruktor
     *
     * @param entityManager objekt za povezivanje i izvršavanje zahvata nad bazom podataka
     */
    public ZaposlenikDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Vraća sve objekte tipa <code>Zaposlenik</code> koji su pohranjeni u bazi.
     *
     * @return lista zaposlenika, null ako baza nema pohranjenog nijednog zoposlenika.
     */
    public List<Zaposlenik> getAll() {
        try {
            String sql = "SELECT * FROM zaposlenici";

            Query query = entityManager.createNativeQuery(sql);
            return (List<Zaposlenik>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Vraća sve objekte tipa <code>Zaposlenik</code> koji su voditelji i koji su pohranjeni u bazi.
     *
     * @return lista voditelja, null ako baza nema pohranjenog nijednog voditelja.
     */
    public List<Zaposlenik> getAllVoditelj() {
        try {
            String sql = "SELECT zaposlenici.* FROM zaposlenici " +
                    "JOIN uloge ON zaposlenici.id_uloga = uloge.id " +
                    "WHERE uloge.naziv = 'ROLE_VODITELJ'";

            Query query = entityManager.createNativeQuery(sql);
            return (List<Zaposlenik>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Vraća objekt tipa <code>Zaposlenik</code> kojem je pridijeljna djelatnost s id-om <code>id</code>.
     * @param id id djelatnosti
     * @return  voditelj, null ako djelatnosti nije pridijeljen voditelj
     */
    public Zaposlenik getVoditeljByDjelatnostId(int id) {
        try {
            String sql = "SELECT * FROM zaposlenici " +
                    "JOIN grupe ON zaposlenici.id = grupe.id_voditelj " +
                    "WHERE grupe.id_djelatnost = :id";

            Query query = entityManager.createNativeQuery(sql, Zaposlenik.class);
            query.setParameter("id", id);

            return (Zaposlenik) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Vraća objekt tipa <code>Zaposlenik</code> kojem je pridijeljn radni zadatak s id-om <code>id</code>.
     * @param id id radnog zadatka
     * @return  voditelj, null ako zadatku nije pridijeljen djelatnik
     */
    public Zaposlenik getDjelatnikByZadatakId(int id) {
        try {
            String sql = "SELECT * FROM zaposlenici " +
                    "JOIN radni_zadatak_zaposlenik  ON zaposlenici.id = radni_zadatak_zaposlenik .id_zaposlenik " +
                    "WHERE radni_zadatak_zaposlenik.id_radni_zadatak  = :id";

            Query query = entityManager.createNativeQuery(sql, Zaposlenik.class);
            query.setParameter("id", id);

            return (Zaposlenik) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Vraća sve objekte tipa <code>Zaposlenik</code> koji su djelatnici i koji su pohranjeni u bazi.
     *
     * @return lista djelatnika, null ako baza nema pohranjenog nijednog djelatnika.
     */
    public List<Zaposlenik> getAllDjelatnik() {
        try {
            String sql = "SELECT zaposlenici.* FROM zaposlenici " +
                    "JOIN uloge ON zaposlenici.id_uloga = uloge.id " +
                    "WHERE uloge.naziv = 'ROLE_DJELATNIK'";

            Query query = entityManager.createNativeQuery(sql);
            return (List<Zaposlenik>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }


    /**
     * Vraća zaposlenika iz baze podataka prema id-u <code>id</code>.
     *
     * @param id id prema kojem se traži zaposlenik
     * @return objekt zaposlenika koji se pronađe, null ako ne postoji takav zaposlenik.
     */
    public Zaposlenik getById(int id) {
        try {
            String sql = "SELECT * FROM zaposlenici " +
                         "WHERE id = :id";
            Query query = entityManager.createNativeQuery(sql, Zaposlenik.class);
            query.setParameter("id", id);

            return (Zaposlenik) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Sprema zaposlenika <code>zaposlenik</code> u bazu podataka.
     *
     * @param zaposlenik <code>zaposlenik</code> koji se sprema u bazu podataka.
     * @return id stvorenog zaposlenika u bazi podataka, 0 ako se ne uspije spremiti.
     */
    public int save(Zaposlenik zaposlenik) {
        try {
            String sql = "INSERT INTO zaposlenici " +
                         "(ime, prezime, radno_vrijeme_sati_poc, radno_vrijeme_sati_kraj, duljina_radnog_vremena, slobodni_dani, id_uloga) " +
                         "VALUES " +
                         "(:ime, :prezime, :radno_vrijeme_sati_poc, :radno_vrijeme_sati_kraj, :duljina_radnog_vremena, :slobodni_dani, :id_uloga) " +
                         "RETURNING id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("ime", zaposlenik.getIme());
            query.setParameter("prezime", zaposlenik.getPrezime());
            query.setParameter("radno_vrijeme_sati_poc", zaposlenik.getRadno_vrijeme_sati_poc());
            query.setParameter("radno_vrijeme_sati_kraj", zaposlenik.getRadno_vrijeme_sati_kraj());
            query.setParameter("duljina_radnog_vremena", zaposlenik.getDuljina_radnog_vremena());
            query.setParameter("slobodni_dani", zaposlenik.getSlobodni_dani());
            query.setParameter("id_uloga", zaposlenik.getId_uloga());

            return (int) query.getSingleResult();
        } catch (NoResultException e) {
            return 0;
        }
    }

    /**
     * Briše objekt zaposlenik u bazi podataka koji ima id jednak danom <code>id</code>-u.
     *
     * @param id id zaposlenika koji se briše iz baze podataka.
     * @return broj uspješno obrisanih zaposlenika.
     */
    public int delete(int id) {
        try {
            String sql = "DELETE FROM zaposlenici WHERE zaposlenici.id = :id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", id);
            return (int) query.executeUpdate();
        } catch (NoResultException e) {
            return 0;
        }
    }

    /**
     * Briše objekt radnih sati u bazi podataka koji ima jedak id_zaposlenika zadanom <code>id_zoposlenik</code> i
     * koji se nalazi u grupi od voditelja koji ima id jednak <code>id_voditelj</code>
     * @param id_zaposlenik id zaposlenika čije objetke radnih sati treba izbrisati
     * @param id_voditelj id voditelja u čijoj grupi mora biti objekt radnih sati izbrisan
     * @return broj uspješno obrisanih radnih sati.
     */
    public int deleteRadniSatiByIdZaposlenikAndIdVoditelj(int id_zaposlenik, int id_voditelj) {
        try {
            String sql = "DELETE FROM radni_sati\n" +
                    "    WHERE id_zaposlenik = :id_zaposlenik AND\n" +
                    "          id_radni_zadatak = (SELECT id FROM radni_zadatci \n" +
                    "                                WHERE id_zaposlenik = :id_zaposlenik AND\n" +
                    "                                      id_djelatnost = (SELECT id_djelatnost FROM grupe WHERE id_voditelj = :id_voditelj))";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id_zaposlenik", id_zaposlenik);
            query.setParameter("id_voditelj", id_voditelj);

            return (int) query.executeUpdate();
        } catch (NoResultException e) {
            return 0;
        }
    }

    /**
     * Briše objekt radnog zadatka u bazi podataka koji ima jedak id_zaposlenika zadanom <code>id_zoposlenik</code> i
     * koji se nalazi u grupi od voditelja koji ima id jednak <code>id_voditelj</code>
     * @param id_zaposlenik id zaposlenika čije objetke radnih zadataka treba izbrisati
     * @param id_voditelj id voditelja u čijoj grupi mora biti objekt radnog zadatka izbrisan
     * @return broj uspješno obrisanih radnih zadataka.
     */
    public int deleteRadniZadatakByIdZaposlenikAndIdVoditelj(int id_zaposlenik, int id_voditelj) {
        try {
            String sql = "DELETE FROM radni_zadatci\n" +
                    "    WHERE id_zaposlenik = :id_zaposlenik AND\n" +
                    "          id_djelatnost = (SELECT id_djelatnost FROM grupe WHERE id_voditelj = :id_voditelj)\n";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id_zaposlenik", id_zaposlenik);
            query.setParameter("id_voditelj", id_voditelj);

            return (int) query.executeUpdate();
        } catch (NoResultException e) {
            return 0;
        }
    }

    /**
     * Briše objekt zaposlenik_grupa u bazi podataka koji ima jedak id_zaposlenika zadanom <code>id_zoposlenik</code> i
     * koji se nalazi u grupi od voditelja koji ima id jednak <code>id_voditelj</code>
     * @param id_zaposlenik id zaposlenika čije objetke zaposlenik_grupa treba izbrisati
     * @param id_voditelj id voditelja u čijoj grupi mora biti objekt zaposlenik_grupa izbrisan
     * @return broj uspješno obrisanih zaposlenik_grupa.
     */
    public int deleteZaposlenikFromGrupa(int id_zaposlenik, int id_voditelj) {
        try {
            String sql = "DELETE FROM zaposlenik_grupa \n" +
                    "    WHERE zaposlenik_grupa.id_zaposlenik = :id_zaposlenik AND\n" +
                    "          zaposlenik_grupa.id_grupa = (SELECT id FROM grupe WHERE id_voditelj = :id_voditelj)";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id_zaposlenik", id_zaposlenik);
            query.setParameter("id_voditelj", id_voditelj);

            return (int) query.executeUpdate();
        } catch (NoResultException e) {
            return 0;
        }
    }

    /**
     * Ažurira podatke o zaposlenicima <code>zaposlenik</code> na temelju id-a.
     *
     * @param zaposlenik zaposlenik kojem se ažuriraju podaci u bazi podataka.
     * @return broj zaposlenika u bazi podataka kojima su se ažurirali podaci.
     */
    public int update(Zaposlenik zaposlenik) {
        try {
            String sql ="UPDATE zaposlenici " +
                    "SET ime = :ime, prezime = :prezime, radno_vrijeme_sati_poc = :radno_vrijeme_sati_poc, " +
                        "radno_vrijeme_sati_kraj = :radno_vrijeme_sati_kraj,  duljina_radnog_vremena = :duljina_radnog_vremena, " +
                        "slobodni_dani = :slobodni_dani, id_uloga = :id_uloga "
                    +   "WHERE id = :id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", zaposlenik.getId());
            query.setParameter("ime", zaposlenik.getIme());
            query.setParameter("prezime", zaposlenik.getPrezime());
            query.setParameter("radno_vrijeme_sati_poc", zaposlenik.getRadno_vrijeme_sati_poc());
            query.setParameter("radno_vrijeme_sati_kraj", zaposlenik.getRadno_vrijeme_sati_kraj());
            query.setParameter("duljina_radnog_vremena", zaposlenik.getDuljina_radnog_vremena());
            query.setParameter("slobodni_dani", zaposlenik.getSlobodni_dani());
            query.setParameter("id_uloga", zaposlenik.getId_uloga());

            return (int) query.executeUpdate();
        }
        catch(NoResultException e){
            return 0;
        }
    }

    /**
     * Dohvaća sve podatke o zaposlenicima <code>zaposlenik</code> čiji voditelj ima dani id.
     * @param id id voditelja svih zaposelnika.
     * @return lista zaposlenika u bazi podataka kojima je voditelj s danim id-om.
     */

    public List<Zaposlenik> getZaposlenikWithIdVoditelj(int id){
        try{
            String sql = "SELECT zaposlenici.* FROM zaposlenici " +
            "JOIN zaposlenik_grupa ON zaposlenik_grupa.id_zaposlenik = zaposlenici.id " +
            "JOIN grupe ON zaposlenik_grupa.id_grupa = grupe.id " +
            "WHERE grupe.id_voditelj = :id";


            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", id);
            System.out.println(id);
            System.out.print(query);
            return (List<Zaposlenik>) query.getResultList();
        }
        catch(NoResultException e){
            return null;
        }
    }

    /**
     * Pridoda zaposlenika sa idZaposlenik grupi kojoj voditelj ima id idVoditeljGrupe
     *
     * @param idVoditeljGrupe id voditelja grupe kojoj pridodajemo zaposlenika
     * @param idZaposlenik id zaposlenika kojeg pridodajemo grupi
     * @return broj izmjenjenih redaka
     */
    public int pridjeliZaposlenikaGrupi(int idVoditeljGrupe, int idZaposlenik){
        try{
            String sql = "insert into zaposlenik_grupa values (:idZaposlenik, " +
                    "(select id from grupe where id_voditelj = :idVoditeljGrupe));";


            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("idVoditeljGrupe", idVoditeljGrupe);
            query.setParameter("idZaposlenik", idZaposlenik);

            return query.executeUpdate();
        }
        catch(NoResultException e){
            return 0;
        }
    }

    /**
     * Vraća objekt zaposlenika preko zadanog id-ja korisnika.
     *
     * @param id id korisnika preko kojeg se dobiva objekt zaposlenika
     * @return objekt zaposlenika.
     */
    public Zaposlenik getZaposlenikByKorisnikId(int id) {
        try {
            String sql = "SELECT * FROM korisnici JOIN zaposlenici " +
                         "ON korisnici.id_zaposlenik = zaposlenici.id " +
                         "WHERE korisnici.id = :id";

            Query query = entityManager.createNativeQuery(sql, Zaposlenik.class);
            query.setParameter("id", id);

            return (Zaposlenik) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Vraca listu listi zaposlenika u kojem je svaka unutarnja lista jedna grupa
     * @return Lista listi zaposlenika
     */
    public List<List<Zaposlenik>> dohvatiListuListiZaposlenika(){
        List<List<Zaposlenik>> retLista = new ArrayList<>();

        try {

            String sql = "SELECT * FROM zaposlenik_grupa";

            Query query = entityManager.createNativeQuery(sql, Zaposlenik_grupa.class);

            List<Zaposlenik_grupa> zaposlenik_grupe = (List<Zaposlenik_grupa>) (query.getResultList());


            //mapa gdje je kljuc id grupe, a value lista zaposlenika
            var res = zaposlenik_grupe.stream().collect(
                    Collectors.groupingBy(
                            Zaposlenik_grupa::getId_grupa,
                            Collectors.mapping(Zaposlenik_grupa::getId_zaposlenik, Collectors.toList())
                    )
            );

            for (var id : res.keySet()) {
                var listaIdZaposlenika = res.get(id);
                ArrayList<Zaposlenik> zaposleniciOveGrupe = new ArrayList<>();
                for (int idZaposlenika : listaIdZaposlenika) {
                    Zaposlenik ovaj = getZaposlenikByKorisnikId(idZaposlenika);
                    if(ovaj != null){
                        zaposleniciOveGrupe.add(ovaj);
                    }
                }
                retLista.add(zaposleniciOveGrupe);

                //dobi voditelja grupe koja ima id = id
                String sqlIdVoditelj = "SELECT grupe.id_voditelj from grupe where grupe.id = :id";
                Query query2 = entityManager.createNativeQuery(sqlIdVoditelj);
                query2.setParameter("id", id);


                var idVoditelja = (Integer) query2.getSingleResult();
                //System.out.println("Id voditelja " + idVoditelja);
                Zaposlenik voditelj = getZaposlenikByKorisnikId(idVoditelja);

                //System.out.println(voditelj);

                zaposleniciOveGrupe.add(0,voditelj);

            }

        } catch (NoResultException e) {
            System.out.println("Nema rezultata");
            return null;
        }   catch (Exception e){
            System.out.println("Exception " + e.getClass() );
        }

        System.out.println(retLista);
        return retLista;
    }
    /**
     * Vraca listu listi zaposlenika u kojem je svaka unutarnja lista jedna grupa kojoj zaposlenik za idZaposlenika pripada
     * @return Lista listi zaposlenika
     */
    public List<List<Zaposlenik>> dohvatiGrupeUKojimaJeZaposlenik(int idZaposlenika){
        List<List<Zaposlenik>>  retLista = new ArrayList<>();
        var sveGrupe = dohvatiListuListiZaposlenika();
        for (var grupa : sveGrupe) {
            for(var zaposlenik : grupa){
                if(zaposlenik.getId() == idZaposlenika){
                    //System.out.println("Nasao");
                    //System.out.println(zaposlenik);
                    retLista.add(grupa);
                    break;
                }
            }
        }
        return retLista;
    }

    /**
     * Vraca zauzetost radnika u satima
     * @param id id radnika
     * @return int koji predstavlja sate zauzeca
     */
    public int getZauzetostDjelatnik(int id){
        int sati = 0;

        RadniZadatakDAO radniZadatakDAO = new RadniZadatakDAO(this.entityManager);
        UsluznaDjelatnostDAO usluznaDjelatnostDAO = new UsluznaDjelatnostDAO(this.entityManager);
        try{

            var radniZadatci = radniZadatakDAO.getAllRadniZadatciByDjelanikId(id);

            for (RadniZadatak zadatak : radniZadatci) {
                try{
                    int cijenaSata = zadatak.getCijena_sata();
                    if(cijenaSata != 0){
                        sati += zadatak.getProcijenjeni_trosak() / zadatak.getCijena_sata();
                    }
                    else{
                        UsluznaDjelatnost djelatnost = usluznaDjelatnostDAO.getById(zadatak.getId_djelatnost());

                        sati += zadatak.getProcijenjeni_trosak() / djelatnost.getCijenaSata();
                    }

                }
                catch (Exception ignored){
                }

            }

        }
        catch (Exception e){
            System.out.println("Greska");
            e.printStackTrace();
        }
        return sati;
    }


}
