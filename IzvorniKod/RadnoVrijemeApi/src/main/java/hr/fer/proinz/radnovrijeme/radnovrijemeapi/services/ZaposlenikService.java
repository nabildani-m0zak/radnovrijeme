package hr.fer.proinz.radnovrijeme.radnovrijemeapi.services;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.daos.ZaposlenikDAO;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.Zaposlenik;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * Poziva ispravan DAO objekt te ispravnu metodu nad njim kako bi se izvršila zatražena akcija.
 */
@Service
public class ZaposlenikService {
    /**
     * Objekt DAO zaposlenika
     */
    private ZaposlenikDAO zaposlenikDAO;

    /**
     * Klasni konstruktor
     *
     * @param zaposlenikDAO object DAO zaposlenika
     */
    public ZaposlenikService(ZaposlenikDAO zaposlenikDAO) {
        this.zaposlenikDAO = zaposlenikDAO;
    }

    /**
     * Vraca sve objekte tipa Zaposlenik iz baze
     *
     * @return lista svih zaposlenika, null ako baza ne sadrzi zaposlenike
     */
    public List<Zaposlenik> getAllZaposlenik() {
        return this.zaposlenikDAO.getAll();
    }

    /**
     * Vraca sve objekte tipa Zaposlenik koji su voditelji iz baze
     *
     * @return lista svih voditelja, null ako baza ne sadrzi voditelje
     */
    public List<Zaposlenik> getAllVoditelj() {
        return this.zaposlenikDAO.getAllVoditelj();
    }

    /**
     * Vraca zaposlenika kojem je pridijeljena odgovarajuća djelatnosti s id-om <code>id</code>.
     *
     * @param id id djelatnosti
     * @return voditelja, null ako djelatnosti nije pridijeljen voditelj
     */
    public Zaposlenik getVoditeljByDjelatnostId(int id) {
        return this.zaposlenikDAO.getVoditeljByDjelatnostId(id);
    }

    /**
     * Vraca zaposlenika kojem je pridijeljena odgovarajuća djelatnosti s id-om <code>id</code>.
     *
     * @param id id radnog zadatka
     * @return voditelja, null ako djelatnosti nije pridijeljen voditelj
     */
    public Zaposlenik getDjelatnikByZadatakId(int id) {
        return this.zaposlenikDAO.getDjelatnikByZadatakId(id);
    }

    /**
     * Vraca sve objekte tipa Zaposlenik koji su djelatnici iz baze
     *
     * @return lista svih Djelatnika, null ako baza ne sadrzi djelatnike
     */
    public List<Zaposlenik> getAllDjelatnik() {
        return this.zaposlenikDAO.getAllDjelatnik();
    }

    /**
     * Vraca Zaposlenika iz baze prema id-u
     *
     * @param id id zaposlenika koji se trazi
     * @return objekt zaposlenika iz baze ukoliko se pronade, ako ne postoji returna null
     */
    public Zaposlenik getZaposlenikById(int id) {
        return this.zaposlenikDAO.getById(id);
    }

    /**
     * Sprema zaposlenika u bazu
     *
     * @param zaposlenik zaposlenik koji se sprema u bazu
     * @return id novostvorenog zaposlenika
     */
    public int saveZaposlenik(Zaposlenik zaposlenik) {
        return this.zaposlenikDAO.save(zaposlenik);
    }

    /**
     * Brise objekt zaposlenika iz baze koji ima id jednak danom
     *
     * @param id id zaposlenika koji se brise iz baze podataka
     * @return broj uspjesno obrisanih zaposlenika
     */
    public int deleteZaposlenik(int id) {
        return this.zaposlenikDAO.delete(id);
    }

    /**
     * Briše objekt zaposlenik_grupa u bazi podataka koji ima jedak id_zaposlenika zadanom <code>id_zoposlenik</code> i
     * koji se nalazi u grupi od voditelja koji ima id jednak <code>id_voditelj</code>
     *
     * @param id_zaposlenik id zaposlenika čije objetke zaposlenik_grupa treba izbrisati
     * @param id_voditelj   id voditelja u čijoj grupi mora biti objekt zaposlenik_grupa izbrisan
     * @return broj uspješno obrisanih zaposlenik_grupa.
     */
    public int deleteZaposlenikFromGrupa(int id_zaposlenik, int id_voditelj) {
        this.zaposlenikDAO.deleteRadniSatiByIdZaposlenikAndIdVoditelj(id_zaposlenik, id_voditelj);
        this.zaposlenikDAO.deleteRadniZadatakByIdZaposlenikAndIdVoditelj(id_zaposlenik, id_voditelj);
        return this.zaposlenikDAO.deleteZaposlenikFromGrupa(id_zaposlenik, id_voditelj);
    }

    /**
     * Azurira podatke o zaposlenicima na temelju id-a
     *
     * @param zaposlenik zaposlenik kojemu se azuriraju podaci u bazi
     * @return broj zaposlenika u bazi kojima su se azurirali podaci
     */
    public int updateZaposlenik(Zaposlenik zaposlenik) {
        return this.zaposlenikDAO.update(zaposlenik);
    }

    /**
     * Dohvaća sve podatke o zaposlenicima <code>zaposlenik</code> čiji voditelj ima dani id.
     *
     * @param id id voditelja svih zaposelnika.
     * @return lista zaposlenika u bazi podataka kojima je voditelj s danim id-om.
     */

    public List<Zaposlenik> getZaposlenikWithIdVoditelj(int id) {
        return this.zaposlenikDAO.getZaposlenikWithIdVoditelj(id);
    }

    /**
     * Pridoda zaposlenika sa idZaposlenik grupi kojoj voditelj ima id idVoditeljGrupe
     *
     * @param idVoditeljGrupe id voditelja grupe kojoj pridodajemo zaposlenika
     * @param idZaposlenik    id zaposlenika kojeg pridodajemo grupi
     * @return broj izmjenjenih redaka
     */
    public int pridjeliZaposlenikaGrupi(int idVoditeljGrupe, int idZaposlenik) {
        return this.zaposlenikDAO.pridjeliZaposlenikaGrupi(idVoditeljGrupe, idZaposlenik);
    }

    /**
     * Vraća objekt zaposlenika preko zadanog id-ja korisnika.
     *
     * @param id id korisnika preko kojeg se dobiva objekt zaposlenika
     * @return objekt zaposlenika.
     */
    public Zaposlenik getZaposlenikByKorisnikId(int id) {
        return zaposlenikDAO.getZaposlenikByKorisnikId(id);
    }

    /**
     * Vraca listu listi zaposlenika u kojem je svaka unutarnja lista jedna grupa
     *
     * @return Lista listi zaposlenika
     */
    public List<List<Zaposlenik>> dohvatiListuListiZaposlenika() {
        return zaposlenikDAO.dohvatiListuListiZaposlenika();
    }

    /**
     * Vraca listu listi zaposlenika u kojem je svaka unutarnja lista jedna grupa kojoj zaposlenik za idZaposlenika pripada
     *
     * @return Lista listi zaposlenika
     */
    public List<List<Zaposlenik>> dohvatiGrupeUKojimaJeZaposlenik(int idZaposlenika) {
        return zaposlenikDAO.dohvatiGrupeUKojimaJeZaposlenik(idZaposlenika);
    }

    /**
     * Vraca zauzetost radnika u satima
     *
     * @param id id radnika
     * @return int koji predstavlja sate zauzeca
     */
    public int getZauzetostDjelatnik(int id) {
        return zaposlenikDAO.getZauzetostDjelatnik(id);
    }
}
