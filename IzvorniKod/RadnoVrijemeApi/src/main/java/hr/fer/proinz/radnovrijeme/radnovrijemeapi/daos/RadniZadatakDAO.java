package hr.fer.proinz.radnovrijeme.radnovrijemeapi.daos;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.*;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.RadniZadatak;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.List;

/**
 * Radi operacije s tablicom radni_zadatci nad bazom podataka
 */
@Repository
@Transactional
public class RadniZadatakDAO implements IGet<RadniZadatak>, IPostUpdateDelete<RadniZadatak> {

    /**
     * Objekt za povezivanje i izvršavanje zahvata nad bazom podataka.
     */
    private final EntityManager entityManager;

    /**
     * Klasni konstruktor.
     *
     * @param entityManager objekt za povezivanje i izvršavanje zahvata nad bazom podataka
     */
    public RadniZadatakDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Vraća sve objekte tipa <code>RadniZadatak</code> koji su pohranjeni u bazi podataka.
     *
     * @return lista radnih zadataka, null ako baza nema nijednog radnog zadatka.
     */
    public List<RadniZadatak> getAll() {
        try {
            String sql = "SELECT * FROM radni_zadatci";

            Query query = entityManager.createNativeQuery(sql);
            return (List<RadniZadatak>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Dohvaćanje radnog zadatka prema id-u.
     *
     * @param id id po kojem se dohvaća radni zadatak.
     * @return objekt tipa <code>RadniZadatak</code> ako postoji, inače null.
     */
    public RadniZadatak getById(int id) {
        try {
            String sql = "SELECT * FROM radni_zadatci " +
                    "WHERE id = :id";

            Query query = entityManager.createNativeQuery(sql, RadniZadatak.class);
            query.setParameter("id", id);

            return (RadniZadatak) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Dohvaćanje svih radnih zadataka koje je napravio voditelj za zadanim id-jem <code>id_voditelj</code>
     *
     * @param id_voditelj id voditelja koji je kreirano tražene radne zadatke.
     * @return lista objekata tipa <code>RadniZadatak</code> ako postoje, inače null.
     */
    public List<RadniZadatak> getAllRadniZadatciCreatedByVoditeljId(int id_voditelj) {
        try {
            String sql = "SELECT * FROM radni_zadatci " +
                    "WHERE id_zaposlenik = :id_zaposlenik";

            Query query = entityManager.createNativeQuery(sql, RadniZadatak.class);
            query.setParameter("id_zaposlenik", id_voditelj);

            return (List<RadniZadatak>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Dohvaćanje svih radnih zadataka koji su pridruženi nekom zaposleniku.
     *
     * @param id_zaposlenik id po kojem se dohvaćaju radni zadatci.
     * @return lista objekata tipa <code>RadniZadatak</code> ako postoje, inače null.
     */
    public List<RadniZadatak> getAllRadniZadatciByDjelanikId(int id_zaposlenik) {
        try {
            String sql = "SELECT * FROM radni_zadatak_zaposlenik JOIN radni_zadatci " +
                    "ON radni_zadatak_zaposlenik.id_radni_zadatak = radni_zadatci.id " +
                    "WHERE radni_zadatak_zaposlenik.id_zaposlenik = :id_zaposlenik AND obavljen IS false";

            Query query = entityManager.createNativeQuery(sql, RadniZadatak.class);
            query.setParameter("id_zaposlenik", id_zaposlenik);

            return (List<RadniZadatak>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Sprema radni zadatak <code>RadniZadatak</code> u bazu podataka.
     *
     * @param radniZadatak <code>radniZadatak</code> koji se sprema u bazu podataka.
     * @return id stvorenog radnog zadatka u bazi podataka, 0 ako se ne uspije spremiti.
     */
    public int save(RadniZadatak radniZadatak) {
        try {
            String sql = "INSERT INTO radni_zadatci (id_djelatnost, id_zaposlenik, opis, cijena_sata, procijenjeni_trosak, obavljen, datum_poc) " +
                    "VALUES (:id_djelatnost, :id_zaposlenik, :opis, :cijena_sata, :procijenjeni_trosak, :obavljen, :datum_poc) " +
                    "RETURNING id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id_djelatnost", radniZadatak.getId_djelatnost());
            query.setParameter("id_zaposlenik", radniZadatak.getId_zaposlenik());
            query.setParameter("opis", radniZadatak.getOpis());
            query.setParameter("cijena_sata", radniZadatak.getCijena_sata());
            query.setParameter("procijenjeni_trosak", radniZadatak.getProcijenjeni_trosak());
            query.setParameter("obavljen", radniZadatak.isObavljen());
            query.setParameter("datum_poc", radniZadatak.getDatum_poc());

            return (int) query.getSingleResult();
        } catch (NoResultException e) {
            return 0;
        }
    }

    /**
     * Povezuje zaposlenika i radni zadatak.
     *
     * @param id_zadatak    id zadatka kojeg se povezuje sa zaposlenikom
     * @param id_zaposlenik id zaposlenika kojeg se povezuje sa zadatkom
     * @return broj uspješno dodanih redaka u tablici radni_zadatak_zaposlenik
     */
    public int connectRadniZadatakWithZaposlenik(int id_zadatak, int id_zaposlenik) {
        try {
            String sql = "INSERT INTO radni_zadatak_zaposlenik (id_radni_zadatak, id_zaposlenik ) " +
                    "VALUES (:id_zadatak, :id_zaposlenik);";
            Query query = entityManager.createNativeQuery(sql, RadniZadatak.class);
            query.setParameter("id_zadatak", id_zadatak);
            query.setParameter("id_zaposlenik", id_zaposlenik);


            return (int) query.executeUpdate();
        } catch (NoResultException e) {
            return 0;
        }
    }

    /**
     * Ažurira podatke o radnim zadatcima <code>RadniZadatak</code> na temelju id-a.
     *
     * @param radniZadatak radni zadatci koji se ažuriranju u bazi podataka.
     * @return broj radnih zadataka u bazi podataka kojima su se ažurirali podaci.
     */
    public int update(RadniZadatak radniZadatak) {
        try {
            String sql = "UPDATE radni_zadatci " +
                    "SET id_djelatnost = :id_djelatnost, id_zaposlenik = :id_zaposlenik, opis = :opis, cijena_sata = :cijena_sata, " +
                    "procijenjeni_trosak = :procijenjeni_trosak, obavljen = :obavljen, datum_poc = :datum_poc " +
                    "WHERE id = :id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", radniZadatak.getId());
            query.setParameter("id_djelatnost", radniZadatak.getId_djelatnost());
            query.setParameter("id_zaposlenik", radniZadatak.getId_zaposlenik());
            query.setParameter("opis", radniZadatak.getOpis());
            query.setParameter("cijena_sata", radniZadatak.getCijena_sata());
            query.setParameter("procijenjeni_trosak", radniZadatak.getProcijenjeni_trosak());
            query.setParameter("obavljen", radniZadatak.isObavljen());
            query.setParameter("datum_poc", radniZadatak.getDatum_poc());

            return (int) query.executeUpdate();
        } catch (NoResultException ex) {
            return 0;
        }
    }

    /**
     * Pridjeljuje radni zadatak zaposleniku.
     *
     * @param id_zadatak    id radnog zadatka
     * @param id_zaposlenik id zaposlenika
     * @return broj uspješno ažuriranih redaka u bazi podataka
     */
    public int update(int id_zadatak, int id_zaposlenik) {
        try {
            String sql = "UPDATE radni_zadatak_zaposlenik " +
                    "SET id_zaposlenik = :id_zaposlenik " +
                    "WHERE id_radni_zadatak  = :id_zadatak";
            Query query = entityManager.createNativeQuery(sql, RadniZadatak.class);
            query.setParameter("id_zadatak", id_zadatak);
            query.setParameter("id_zaposlenik", id_zaposlenik);


            return (int) query.executeUpdate();
        } catch (NoResultException e) {
            return 0;
        }
    }

    /**
     * Briše objekt radnog zadatka u bazi podataka koji ima id jednak zadanom <code>id</code>-u.
     *
     * @param id id radnog zadatka koji se briše iz baze podataka.
     * @return broj uspješno obrisanih radnih zadataka.
     */
    public int delete(int id) {
        try {
            String sql = "DELETE FROM radni_zadatci " +
                    "WHERE radni_zadatci.id = :id";

            Query query = entityManager.createNativeQuery(sql);
            query.setParameter("id", id);

            return (int) query.executeUpdate();
        } catch (NoResultException e) {
            return 0;
        }
    }

    /**
     * Vraca radni zadatak iz baze podataka prema id-u <code>id</code> kojem se mjenja status obavljenosti.
     *
     * @param id id prema kojem se traži radni zadatak
     * @return 0 ako nije promijenjen, ostalo ako jest.
     */
    public int updateZadatakObavljen(int id) {
        try {
            String sql = "UPDATE radni_zadatci " +
                    "SET obavljen = true " +
                    "WHERE id = :id";
            Query query = entityManager.createNativeQuery(sql, RadniZadatak.class);
            query.setParameter("id", id);

            return (int) query.executeUpdate();
        } catch (NoResultException e) {
            return 0;
        }
    }

    /**
     * Dohvaćanje svih obavljenih radnih zadataka koji su pridruženi nekom zaposleniku.
     *
     * @param id_zaposlenik id po kojem se dohvaćaju radni zadatci.
     * @return lista objekata tipa <code>RadniZadatak</code> ako postoje, inače null.
     */
    public List<RadniZadatak> getAllObavljeniRadniZadatciByZaposlenikId(int id_zaposlenik) {
        try {
            String sql = "SELECT radni_zadatci.* FROM zaposlenici JOIN radni_zadatci " +
                    "ON radni_zadatci.id_zaposlenik = zaposlenici.id " +
                    "WHERE zaposlenici.id = :id_zaposlenik AND obavljen IS true";

            Query query = entityManager.createNativeQuery(sql, RadniZadatak.class);
            query.setParameter("id_zaposlenik", id_zaposlenik);

            return (List<RadniZadatak>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Dohvaća dio planiranog troška na razini poduzeća ako je definirana cijena pojedinog zadatka.
     *
     * @return planirani trošak poduzeća.
     */
    public BigInteger getPlaniraniTrosak() {
        try {
            String sql = "SELECT SUM(procijenjeni_trosak) " +
                    "FROM radni_zadatci " +
                    "WHERE cijena_sata <> 0 " +
                    "and datum_poc >= CURRENT_DATE - integer '30';";

            Query query = entityManager.createNativeQuery(sql);

            return query.getSingleResult() == null ? BigInteger.valueOf(0) : (BigInteger) query.getSingleResult();
        } catch (NoResultException ex) {
            return BigInteger.valueOf(0);
        }
    }

    /**
     * Dohvaća dio realiziranog troška na razini poduzeća ako je definirana cijena pojedinog zadatka.
     *
     * @return realizirani trošak poduzeća.
     */
    public BigInteger getRealiziraniTrosak() {
        try {
            String sql = "SELECT SUM(duljina_rada * cijena_sata) FROM radni_sati s, radni_zadatci z " +
                    "WHERE s.id_radni_zadatak = z.id and cijena_sata <> 0 " +
                    "and datum >= CURRENT_DATE - integer '30';";

            Query query = entityManager.createNativeQuery(sql);

            return query.getSingleResult() == null ? BigInteger.valueOf(0) : (BigInteger) query.getSingleResult();
        } catch (NoResultException ex) {
            return BigInteger.valueOf(0);
        }
    }

    /**
     * Dohvaća dio realiziranog trošaka na razini poduzeća ako nije definirana cijena pojedinog zadatka.
     *
     * @return realizirani trošak poduzeća.
     */
    public BigInteger getRealiziraniTrosakAkoCijenaNijeDefinirana() {
        try {
            String sql = "select SUM(duljina_rada * u.cijena_sata) FROM radni_sati s, radni_zadatci z, usluzne_djelatnosti u " +
                    "WHERE s.id_radni_zadatak = z.id and z.id_djelatnost = u.id and z.cijena_sata = 0 " +
                    "and datum >= CURRENT_DATE - integer '30';";

            Query query = entityManager.createNativeQuery(sql);

            return query.getSingleResult() == null ? BigInteger.valueOf(0) : (BigInteger) query.getSingleResult();
        } catch (NoResultException ex) {
            return BigInteger.valueOf(0);
        }
    }

    /**
     * Dohvaća neobavljene zadatke do tog trenutka. Podrazumijeva se da ukoliko je zadatak zadan za neki
     *  sljedeći dan, da ga nije još obavio.
     * @return lista neobavljenih zadataka
     */

    public List<RadniZadatak> getAllNeobavljeniRadniZadatakByDjelatnikId(int id) {
        try {
            String sql = "SELECT radni_zadatci.* FROM radni_zadatci " +
                    "WHERE id_zaposlenik = :id AND obavljen IS false " +
                    "AND datum_poc <= CURRENT_DATE " +
                    "ORDER BY datum_poc";

            Query query = entityManager.createNativeQuery(sql, RadniZadatak.class);
            query.setParameter("id", id);

            return (List <RadniZadatak>) query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }
}
