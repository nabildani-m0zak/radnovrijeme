package hr.fer.proinz.radnovrijeme.radnovrijemeapi.controllers;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.*;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.RadniZadatak;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.services.RadniZadatakService;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

/**
 * Odgovara na sve zahtjeve povezane s Radnim Zadatcima.
 */
@RestController
public class RadniZadatakController implements IGet<RadniZadatak> {

    /**
     * Servis kojem controller prosljeđuje zahtjeve.
     */
    private final RadniZadatakService radniZadatakService;

    /**
     * Klasni konstruktor.
     *
     * @param radniZadatakService servis kojem controller prosljeđuje zahtjeve.
     */
    public RadniZadatakController(RadniZadatakService radniZadatakService) {
        this.radniZadatakService = radniZadatakService;
    }

    /**
     * Vraća sve objekte tipa <code>RadniZadatak</code> koji su pohranjeni u bazi podataka.
     *
     * @return lista radnih zadataka, null ako baza nema nijednog radnog zadatka.
     */
    @GetMapping("/radniZadatak")
    public List<RadniZadatak> getAll() {
        return radniZadatakService.getAllRadniZadatak();
    }

    /**
     * Dohvaćanje radnog zadatka prema id-u.
     *
     * @param id id po kojem se dohvaća radni zadatak.
     * @return objekt tipa <code>RadniZadatak</code> ako postoji, inače null.
     */
    @GetMapping("/radniZadatak/{id}")
    public RadniZadatak getById(@PathVariable int id) {
        return radniZadatakService.getRadniZadatakById(id);
    }

    /**
     * Dohvaćanje svih radnih zadataka koje je napravio voditelj za zadanim id-jem <code>id_voditelj</code>
     *
     * @param id id voditelja koji je kreirano tražene radne zadatke.
     * @return lista objekata tipa <code>RadniZadatak</code> ako postoje, inače null.
     */
    @GetMapping("/radniZadatak/voditelj/{id}")
    public List<RadniZadatak> getAllRadniZadatciCreatedByVoditeljId(@PathVariable int id) {
        return radniZadatakService.getAllRadniZadatciCreatedByVoditeljId(id);
    }

    /**
     * Dohvaćanje svih radnih zadataka koji su pridruženi nekom zaposleniku.
     *
     * @param id id po kojem se dohvaćaju radni zadatci.
     * @return lista objekata tipa <code>RadniZadatak</code> ako postoje, inače null.
     */
    @GetMapping("/radniZadatak/djelatnik/{id}")
    public List<RadniZadatak> getAllRadniZadatciByDjelanikId(@PathVariable int id) {
        return radniZadatakService.getAllRadniZadatciByDjelanikId(id);
    }

    /**
     * Sprema radni zadatak <code>radniZadatak</code> u bazu podataka.
     *
     * @param radniZadatak <code>radniZadatak</code> koji se sprema u bazu podataka.
     * @return id stvorenog radnog zadatka u bazi podataka, 0 ako se ne uspije spremiti.
     */
    @PostMapping("/radniZadatak/zaposlenik/{id}")
    public int save(@PathVariable int id, @RequestBody RadniZadatak radniZadatak) {
        return radniZadatakService.save(radniZadatak, id);
    }

    /**
     * Ažurira podatke o radnim zadatcima <code>RadniZadatak</code> na temelju id-a.
     *
     * @param radniZadatak radni zadatci koji se ažuriranju u bazi podataka.
     * @return broj radnih zadataka u bazi podataka kojima su se ažurirali podaci.
     */
    @PutMapping("/radniZadatak")
    public int update(@RequestBody RadniZadatak radniZadatak) {
        return radniZadatakService.updateRadniZadatak(radniZadatak);
    }

    /**
     * Pridjeljuje radni zadatak zaposleniku.
     *
     * @param id_zadatak   id radnog zadatka
     * @param id_djelatnik id zaposlenika
     * @return broj uspješno ažuriranih redaka u bazi podataka
     */
    @PutMapping("/radniZadatak/{id_zadatak}/djelatnik/{id_djelatnik}")
    public int update(@PathVariable int id_zadatak, @PathVariable int id_djelatnik) {
        return this.radniZadatakService.updateRadniZadatak(id_zadatak, id_djelatnik);
    }

    /**
     * Briše objekt radnog zadatka u bazi podataka koji ima id jednak zadanom <code>id</code>-u.
     *
     * @param id id radnog zadatka koji se briše iz baze podataka.
     * @return broj uspješno obrisanih radnih zadataka.
     */
    @DeleteMapping("/radniZadatak/{id}")
    public int delete(@PathVariable int id) {
        return radniZadatakService.deleteRadniZadatak(id);
    }

    /**
     * Označava zadatak kao obavljen.
     *
     * @param id jedinstveni identifikator radnog zadatka.
     * @return 0 ako nije promijenjen, ostalo ako jest.
     */
    @PutMapping("/radniZadatak/{id}")
    public int updateZadatakObavljen(@PathVariable int id) {
        return this.radniZadatakService.updateZadatakObavljen(id);
    }

    /**
     * Dohvaćanje svih obavljenih radnih zadataka koji su pridruženi nekom zaposleniku.
     *
     * @param id id po kojem se dohvaćaju radni zadatci.
     * @return lista objekata tipa <code>RadniZadatak</code> ako postoje, inače null.
     */
    @GetMapping("/obavljenRadniZadatak/zaposlenik/{id}")
    public List<RadniZadatak> getAllRadniZadatciByZaposlenikId(@PathVariable int id) {
        return radniZadatakService.getAllObavljeniRadniZadatciByZaposlenikId(id);
    }

    /**
     * Dohvaćanje planirani trošak na razini poduzeća.
     *
     * @return planirani trošak.
     */
    @GetMapping("/planiraniTrosak")
    public BigInteger getPlaniraniTrosak() {
        return radniZadatakService.getPlaniraniTrosak();
    }

    /**
     * Dohvaćanje realizirani trošak na razini poduzeća.
     *
     * @return realizirani trošak.
     */
    @GetMapping("/realiziraniTrosak")
    public BigInteger getRealiziraniTrosak() {
        return radniZadatakService.getRealiziraniTrosak();
    }

    /**
     * Dohvaćanje svih neobavljenih radnih zadataka zaposlenika do tog trenutka.
     *
     * @param id id zaposlenika po kojem se dohvaćaju radni zadatci.
     * @return lista objekata tipa <code>RadniZadatak</code> ako postoje, inače null.
     */

    @GetMapping("/neobavljenRadniZadatak/djelatnik/{id}")
    public List<RadniZadatak> getAllNeobavljeniRadniZadatakByDjelanikId(@PathVariable int id) {
        return radniZadatakService.getAllNeobavljeniRadniZadatakByDjelanikId(id);
    }

}
