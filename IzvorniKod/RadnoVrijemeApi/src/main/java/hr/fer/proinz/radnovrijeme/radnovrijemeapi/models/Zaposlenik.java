package hr.fer.proinz.radnovrijeme.radnovrijemeapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Klasa za spremanje podataka pojedinog zaposlenika
 * Sadrzi svoj vlastiti id, ime, prezie, radno vrijeme pocetak i kraj, duljinu radnog vremena,
 * preostale slobodne dane i ulogu
 */
@Table(name = "zaposlenici")
@Entity
public class Zaposlenik {
    @Id
    private int id;

    @Column(nullable = false)
    private String ime;

    @Column(nullable = false)
    private String prezime;

    @Column(nullable = false)
    private int radno_vrijeme_sati_poc;

    @Column(nullable = false)
    private int radno_vrijeme_sati_kraj;

    @Column(nullable = false)
    private int duljina_radnog_vremena;

    @Column(nullable = false)
    private int slobodni_dani;

    @Column(nullable = false)
    private int id_uloga;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public int getRadno_vrijeme_sati_poc() {
        return radno_vrijeme_sati_poc;
    }

    public void setRadno_vrijeme_sati_poc(int radno_vrijeme_sati_poc) {
        this.radno_vrijeme_sati_poc = radno_vrijeme_sati_poc;
    }

    public int getRadno_vrijeme_sati_kraj() {
        return radno_vrijeme_sati_kraj;
    }

    public void setRadno_vrijeme_sati_kraj(int radno_vrijeme_sati_kraj) {
        this.radno_vrijeme_sati_kraj = radno_vrijeme_sati_kraj;
    }

    public int getDuljina_radnog_vremena() {
        return duljina_radnog_vremena;
    }

    public void setDuljina_radnog_vremena(int duljina_radnog_vremena) {
        this.duljina_radnog_vremena = duljina_radnog_vremena;
    }

    public int getSlobodni_dani() {
        return slobodni_dani;
    }

    public void setSlobodni_dani(int slobodni_dani) {
        this.slobodni_dani = slobodni_dani;
    }

    public int getId_uloga() {
        return id_uloga;
    }

    public void setId_uloga(int id_uloga) {
        this.id_uloga = id_uloga;
    }


    @Override
    public String toString() {
        return "Zaposlenik{" +
                "id=" + id +
                ", ime='" + ime + '\'' +
                ", prezime='" + prezime + '\'';
    }
}
