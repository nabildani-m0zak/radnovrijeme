package hr.fer.proinz.radnovrijeme.radnovrijemeapi.services;

import hr.fer.proinz.radnovrijeme.radnovrijemeapi.daos.RadniZadatakDAO;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.*;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.RadniZadatak;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import java.math.BigInteger;
import java.util.List;

/**
 * Poziva ispravan DAO objekt te ispravnu metodu nad njim kako bi se izvršila zatražena akcija.
 */
@Service
public class RadniZadatakService {

    /**
     * Objekt DAO radnog zadatka.
     */
    private final RadniZadatakDAO radniZadatakDAO;

    /**
     * Klasni konstruktor.
     *
     * @param radniZadatakDAO DAO objekt radnog zadatka.
     */
    public RadniZadatakService(RadniZadatakDAO radniZadatakDAO) {
        this.radniZadatakDAO = radniZadatakDAO;
    }

    /**
     * Vraća sve objekte tipa <code>RadniZadatak</code> koji su pohranjeni u bazi podataka.
     *
     * @return lista radnih zadataka, null ako baza nema nijednog radnog zadatka.
     */
    public List<RadniZadatak> getAllRadniZadatak() {
        return radniZadatakDAO.getAll();
    }

    /**
     * Dohvaćanje radnog zadatka prema id-u.
     *
     * @param id id po kojem se dohvaća radni zadatak.
     * @return objekt tipa <code>RadniZadatak</code> ako postoji, inače null.
     */
    public RadniZadatak getRadniZadatakById(int id) {
        return radniZadatakDAO.getById(id);
    }

    /**
     * Dohvaćanje svih radnih zadataka koje je napravio voditelj za zadanim id-jem <code>id_voditelj</code>
     *
     * @param id_voditelj id voditelja koji je kreirano tražene radne zadatke.
     * @return lista objekata tipa <code>RadniZadatak</code> ako postoje, inače null.
     */
    public List<RadniZadatak> getAllRadniZadatciCreatedByVoditeljId(int id_voditelj) {
        return radniZadatakDAO.getAllRadniZadatciCreatedByVoditeljId(id_voditelj);
    }

    /**
     * Dohvaćanje svih radnih zadataka koji su pridruženi nekom zaposleniku.
     *
     * @param id_zaposlenik id po kojem se dohvaćaju radni zadatci.
     * @return lista objekata tipa <code>RadniZadatak</code> ako postoje, inače null.
     */
    public List<RadniZadatak> getAllRadniZadatciByDjelanikId(int id_zaposlenik) {
        return radniZadatakDAO.getAllRadniZadatciByDjelanikId(id_zaposlenik);
    }

    /**
     * Sprema radni zadatak <code>radniZadatak</code> u bazu podataka.
     *
     * @param radniZadatak <code>radniZadatak</code> koji se sprema u bazu podataka.
     * @return id stvorenog radnog zadatka u bazi podataka, 0 ako se ne uspije spremiti.
     */
    public int save(RadniZadatak radniZadatak, int id_zaposlenik) {
        int id_zadatak = radniZadatakDAO.save(radniZadatak);
        return radniZadatakDAO.connectRadniZadatakWithZaposlenik(id_zadatak, id_zaposlenik);
    }

    /**
     * Briše objekt radnog zadatka u bazi podataka koji ima id jednak zadanom <code>id</code>-u.
     *
     * @param id id radnog zadatka koji se briše iz baze podataka.
     * @return broj uspješno obrisanih radnih zadataka.
     */
    public int deleteRadniZadatak(int id) {
        return radniZadatakDAO.delete(id);
    }

    /**
     * Ažurira podatke o radnim zadatcima <code>RadniZadatak</code> na temelju id-a.
     *
     * @param radniZadatak radni zadatci koji se ažuriranju u bazi podataka.
     * @return broj radnih zadataka u bazi podataka kojima su se ažurirali podaci.
     */
    public int updateRadniZadatak(RadniZadatak radniZadatak) {
        return radniZadatakDAO.update(radniZadatak);
    }

    /**
     * Pridjeljuje radni zadatak zaposleniku.
     *
     * @param id_zadatak    id radnog zadatka
     * @param id_zaposlenik id zaposlenika
     * @return broj uspješno ažuriranih redaka u bazi podataka
     */
    public int updateRadniZadatak(int id_zadatak, int id_zaposlenik) {
        return this.radniZadatakDAO.update(id_zadatak, id_zaposlenik);
    }

    /**
     * Vraća radni zadatak kojem je status obavljenosti izmijenjen
     *
     * @param id jedinstveni identifikator radnog zadatka kojem se mijenja status.
     * @return 0 ako nije promijenjen, ostalo ako jest.
     */
    public int updateZadatakObavljen(int id) {
        return this.radniZadatakDAO.updateZadatakObavljen(id);
    }

    /**
     * Dohvaćanje svih obavljenih radnih zadataka koji su pridruženi nekom zaposleniku.
     *
     * @param id_zaposlenik id po kojem se dohvaćaju radni zadatci.
     * @return lista objekata tipa <code>RadniZadatak</code> ako postoje, inače null.
     */
    public List<RadniZadatak> getAllObavljeniRadniZadatciByZaposlenikId(int id_zaposlenik) {
        return radniZadatakDAO.getAllObavljeniRadniZadatciByZaposlenikId(id_zaposlenik);
    }

    /**
     * Vraća planirani radni trošak.
     *
     * @return planirani trošak.
     */
    public BigInteger getPlaniraniTrosak() {
        return this.radniZadatakDAO.getPlaniraniTrosak();
    }

    /**
     * Vraća realizirani radni trošak.
     *
     * @return realizirani trošak.
     */
    public BigInteger getRealiziraniTrosak() {
        return this.radniZadatakDAO.getRealiziraniTrosakAkoCijenaNijeDefinirana().add(this.radniZadatakDAO.getRealiziraniTrosak());
    }


    /**
     * Vraća sve neobavljene zadatke za zadanog zaposlenika.
     *  Prikazuju se neobavljeni zadatci do tog trenutka
     * @return neobavljene zadatke.
     */

    public List<RadniZadatak> getAllNeobavljeniRadniZadatakByDjelanikId(int id) {
        return this.radniZadatakDAO.getAllNeobavljeniRadniZadatakByDjelatnikId(id);
    }
}
