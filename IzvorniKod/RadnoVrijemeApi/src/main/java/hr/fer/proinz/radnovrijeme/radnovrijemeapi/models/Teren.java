package hr.fer.proinz.radnovrijeme.radnovrijemeapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Klasa za spremanje adresa koji je pojedini zaposlenik obišao
 * Sadrzi datum odlaska na teren, id zaposlenika i id adrese
 */

@Table(name = "teren")
@Entity
public class Teren {
    @Id
    Date datum;

    @Column(nullable = false)
    int id_zaposlenik;

    @Column(nullable = false)
    int id_adresa;

    public Date getDatum() {
        return datum;
    }

    public int getId_zaposlenik() {
        return id_zaposlenik;
    }

    public int getId_adresa() {
        return id_adresa;
    }
}
