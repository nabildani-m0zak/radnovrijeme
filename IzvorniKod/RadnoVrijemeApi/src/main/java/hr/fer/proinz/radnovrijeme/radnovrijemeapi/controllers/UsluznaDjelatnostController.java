package hr.fer.proinz.radnovrijeme.radnovrijemeapi.controllers;


import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IGet;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.interfaces.IPostUpdateDelete;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.models.UsluznaDjelatnost;
import hr.fer.proinz.radnovrijeme.radnovrijemeapi.services.UsluznaDjelatnostService;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

/**
 * Odgovara na sve zahtjeve povezane s <code>Usluzna Djelatnost</code>-i.
 */
@RestController
//@RequestMapping(value = "/usluznaDjelatnost") pa onda netreba svugdje taj '/usluzne_djelatnosti' pisati.
public class UsluznaDjelatnostController implements IGet<UsluznaDjelatnost>, IPostUpdateDelete<UsluznaDjelatnost> {

    /**
     * Servis kojem controller prosljeđuje zahtjeve.
     */
    private final UsluznaDjelatnostService usluznaDjelatnostService;

    /**
     * Klasni konstruktor.
     *
     * @param usluznaDjelatnostService servis kojem controller prosljeđuje zahtjeve.
     */
    public UsluznaDjelatnostController(UsluznaDjelatnostService usluznaDjelatnostService) {
        this.usluznaDjelatnostService = usluznaDjelatnostService;
    }

    /**
     * Vraća listu svih uslužnih djelatnosti.
     *
     * @return lista svih uslužnih djelatnosti.
     */
    @GetMapping("/usluznaDjelatnost")
    public List<UsluznaDjelatnost> getAll() {
        return this.usluznaDjelatnostService.getAllUsluznaDjelatnost();
    }

    /**
     * Vraća uslužnu djelatnosti prema zadanom jedinstvenom identifikatoru.
     *
     * @param id jedinstveni identifikator uslužne djelatnosti.
     * @return uslužnu djelatnost.
     */
    @GetMapping("/usluznaDjelatnost/{id}")
    public UsluznaDjelatnost getById(@PathVariable int id) {
        return this.usluznaDjelatnostService.getUsluznaDjelatnostById(id);
    }

    /**
     * Vraća uslužnu djelatnosti prema zadanom jedinstvenom identifikatoru voditelja.
     *
     * @param id jedinstveni identifikator uslužne djelatnosti.
     * @return uslužnu djelatnost.
     */
    @GetMapping("/usluznaDjelatnost/voditelj/{id}")
    public UsluznaDjelatnost getByIdVoditelj(@PathVariable int id) {
        return this.usluznaDjelatnostService.getUsluznaDjelatnostByIdVoditelj(id);
    }

    /**
     * Preko @RequestBody-a prima jednu uslužnu djelatnost te ju upisuje u tablicu
     * i vraća id unesene.
     *
     * @param usluznaDjelatnost jedna uslužna djelatnost za unos u tablicu.
     * @return id unesene uslužne djelatnosti.
     */
    @PostMapping("/usluznaDjelatnost")
    public int save(@RequestBody UsluznaDjelatnost usluznaDjelatnost) {
        return this.usluznaDjelatnostService.saveUsluznaDjelatnost(usluznaDjelatnost);
    }

    /**
     * Preko @RequestBody-a prima jednu ili više uslužnih djelatnosti te ih mijenja u tablici
     * i vraća broj promjenjenih.
     *
     * @param usluznaDjelatnost jedna ili više uslužnih djelatnosti za promjenu u tablici.
     * @return broj promjenjenih uslužnih djelatnosti.
     */
    @PutMapping("/usluznaDjelatnost")
    public int update(@RequestBody UsluznaDjelatnost usluznaDjelatnost) {
        return this.usluznaDjelatnostService.updateUsluznaDjelatnost(usluznaDjelatnost);
    }

    /**
     * Prima jedan id uslužne djelatnosti te ju briše u tablici
     * i vraća broj izbrisanih.
     *
     * @param id jedan ili više id-eva uslužnih djelatnosti za brisanje u tablici.
     * @return broj izbrisanih uslužnih djelatnosti.
     */
    @DeleteMapping("/usluznaDjelatnost/{id}")
    public int delete(@PathVariable int id) {
        return this.usluznaDjelatnostService.deleteUsluznaDjelatnost(id);
    }

    /**
     * Prima id djelatnosti i voditelja te u tablici grupe gdje je id djelatnosti jednak poslanome
     * mijenja id voditelja na dani id
     *
     * @param id_djelatnost id te djelatnosti
     * @param id_voditelj   id tog voditelja
     * @return broj promijenjenih redaka
     */
    @PostMapping("/usluznaDjelatnost/{id_djelatnost}/voditelj/{id_voditelj}")
    public int pridjeliVoditeljaDjelatnosti(@PathVariable int id_djelatnost, @PathVariable int id_voditelj) {

        return this.usluznaDjelatnostService.pridjeliVoditeljaDjelatnosti(id_djelatnost, id_voditelj);
    }

    /**
     * Prima id voditelja grupe <code>id_voditelj</code> i vraća planirani trošak grupe koju on vodi
     * @param id_voditelj <code>id</code> voditelja grupe za koju nas zanima trošak
     * @return planirani trošak grupe
     */
    @GetMapping("/planiraniTrosak/{id_voditelj}")
    public BigInteger getPlaniraniTrosakGrupe(@PathVariable int id_voditelj) {
        return this.usluznaDjelatnostService.getPlaniraniTrosakGrupe(id_voditelj);
    }

    /**
     * Prima id voditelja grupe <code>id_voditelj</code> i vraća realizirani trošak grupe koju on vodi
     * @param id_voditelj <code>id</code> voditelja grupe za koju nas zanima trošak
     * @return realizirani trošak grupe
     */
    @GetMapping("/realiziraniTrosak/{id_voditelj}")
    public BigInteger getRealiziraniTrosakGrupe(@PathVariable int id_voditelj) {
        return this.usluznaDjelatnostService.getRealiziraniTrosakGrupe(id_voditelj);
    }

}
