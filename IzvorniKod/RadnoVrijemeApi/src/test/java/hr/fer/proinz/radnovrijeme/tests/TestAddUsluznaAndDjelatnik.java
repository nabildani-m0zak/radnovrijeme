package hr.fer.proinz.radnovrijeme.tests;
import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.w3c.dom.html.HTMLInputElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestAddUsluznaAndDjelatnik {
    @Test
    public void testAddNew() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://nabildanim0zak.herokuapp.com/login");
        WebElement element = driver.findElement(By.cssSelector("input[type='text']"));
        element.sendKeys("DPetrovic");
        element = driver.findElement(By.cssSelector("input[type='password']"));
        element.sendKeys("123");
        driver.findElement(By.cssSelector("button[type='button']")).click();
        try {
            Thread.sleep(1000);
        } catch(InterruptedException ex) {
        }
        List<WebElement> elements = driver.findElements(By.cssSelector("button"));
        for (WebElement e : elements) {
            if (e.getText().contains("Dodaj")) {
                e.click();
                break;
            }
        }
        element = driver.findElement(By.cssSelector("input[type='text']"));
        element.sendKeys("Pranje prozora");
        element = driver.findElement(By.cssSelector("textarea[type='text']"));
        element.sendKeys("Pranje i ribanje prozora.");
        element = driver.findElement(By.cssSelector("input[type='number']"));
        element.sendKeys("15");
        driver.findElement(By.cssSelector("button[type='submit']")).click();
        try {
            Thread.sleep(1000);
        } catch(InterruptedException ex) {
        }
        boolean compRes= false;
        elements = driver.findElements(By.cssSelector("p"));
        for (WebElement e : elements) {
            System.out.println(e.getText());
            if (e.getText().contains("Podatci uspješno spremljeni")) {
                compRes = true;
                break;
            }
        }
        assertEquals(compRes, true);

        driver.quit();

    }

    @Test
    public void addNewZap() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://nabildanim0zak.herokuapp.com/login");

        WebElement element = driver.findElement(By.cssSelector("input[type='text']"));
        element.sendKeys("DPetrovic");
        element = driver.findElement(By.cssSelector("input[type='password']"));
        element.sendKeys("123");
        try {
            Thread.sleep(1000);
        } catch(InterruptedException ex) {
        }

        driver.findElement(By.cssSelector("button[type='button']")).click();


        driver.findElement(By.cssSelector("button[type='button']")).click();
        try {
            Thread.sleep(1000);
        } catch(InterruptedException ex) {
        }
        driver.findElement(By.cssSelector("button[class='dropdown-item']")).click();
        List<WebElement> elements = driver.findElements(By.cssSelector("button"));
        for (WebElement e : elements) {
            if (e.getText().contains("Dodaj zaposlenika")) {
                e.click();
                break;
            }
        }
        element = driver.findElement(By.cssSelector("input[id='ime']"));
        element.sendKeys("Josip");
        element = driver.findElement(By.cssSelector("input[id='prezime']"));
        element.sendKeys("Jarni");
        element = driver.findElement(By.cssSelector("input[id='radno_vrijeme_poc']"));
        element.sendKeys("7");
        element = driver.findElement(By.cssSelector("input[id='radno_vrijeme_sati_kraj']"));
        element.sendKeys("15");
        element = driver.findElement(By.cssSelector("input[id='duljina_radnog_vremena']"));
        element.sendKeys("8");
        element = driver.findElement(By.cssSelector("input[id='slobodni_dani']"));
        element.sendKeys("14");
        element = driver.findElement(By.cssSelector("input[id='username']"));
        element.sendKeys("josip27");
        element = driver.findElement(By.cssSelector("input[id='password']"));
        element.sendKeys("123");
        element = driver.findElement(By.cssSelector("select[name='uloga']"));
        element.sendKeys("ROLE_VODITELJ");
        driver.findElement(By.cssSelector("button[type='submit']")).click();
        boolean compRes= false;
        elements = driver.findElements(By.cssSelector("p"));
        for (WebElement e : elements) {
            System.out.println(e.getText());
            if (e.getText().contains("Podatci uspješno spremljeni")) {
                compRes = true;
                break;
            }
        }

        assertEquals(compRes, false);


        driver.quit();

    }
}
