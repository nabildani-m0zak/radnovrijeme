package hr.fer.proinz.radnovrijeme.tests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ZaposlenikTest {
    @Test
    public void urediZaposlenika() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://nabildanim0zak.herokuapp.com/login");

        WebElement element = driver.findElement(By.cssSelector("input[type='text']"));
        element.sendKeys("DPetrovic");
        element = driver.findElement(By.cssSelector("input[type='password']"));
        element.sendKeys("123");
        driver.findElement(By.cssSelector("button[type='button']")).click();

        try {
            TimeUnit.SECONDS.sleep(1);
            //zaposlenici
            WebElement button = driver.findElement(By.cssSelector("#dropdownMenu2"));
            button.click();

            TimeUnit.SECONDS.sleep(1);
            button = driver.findElement(
                    By.cssSelector("body > app-root > app-header > nav > div > div.d-flex > app-dropdown-menu > div > ul > a:nth-child(1) > li > button"));
            button.click();
            TimeUnit.SECONDS.sleep(1);
            //prvi zaposlenik
            button = driver.findElement(By.cssSelector("#card > div"));
            button.click();
            TimeUnit.SECONDS.sleep(1);
            element = driver.findElement(By.cssSelector("#ime"));
            element.clear();
            element.sendKeys("Neko novo ime");
            element = driver.findElement(By.cssSelector("#password"));
            element.clear();
            element.sendKeys("Neka nova zaporka");

            //izmjeni gumb
            button = driver.findElement(By.cssSelector("body > app-root > div > app-zaposlenici-form > div > form > div.col-1 > button"));
            button.click();


            WebElement labela = driver.findElement(By.cssSelector("body > app-root > div > app-zaposlenici-form > div > form > p"));
            assertEquals("Podatci uspješno spremljeni.", labela.getText().trim());
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
