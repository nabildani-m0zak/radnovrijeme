package hr.fer.proinz.radnovrijeme.tests;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

public class UsluznaDjelatnostTest {
    @Test
    public void deleteUsluznaDjelatnost() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://nabildanim0zak.herokuapp.com/login");

        WebElement element = driver.findElement(By.cssSelector("input[type='text']"));
        element.sendKeys("DPetrovic");
        element = driver.findElement(By.cssSelector("input[type='password']"));
        element.sendKeys("123");
        driver.findElement(By.cssSelector("button[type='button']")).click();


        int numberOfElementsBefore = driver.findElements(By.cssSelector("h4.card-title")).size();

        WebElement deleteButton = driver.findElement(By.cssSelector("div.col-3>button"));
        deleteButton.click();
        WebElement confirmButton = driver.findElement(By.cssSelector("button.btn-success"));
        confirmButton.click();

        try {
            TimeUnit.SECONDS.sleep(3);
            int numberOfElementsAfter = driver.findElements(By.cssSelector("h4.card-title")).size();
            assertEquals(numberOfElementsAfter, numberOfElementsBefore - 1);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void urediDjelatnost() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://nabildanim0zak.herokuapp.com/login");

        WebElement element = driver.findElement(By.cssSelector("input[type='text']"));
        element.sendKeys("DPetrovic");
        element = driver.findElement(By.cssSelector("input[type='password']"));
        element.sendKeys("123");
        driver.findElement(By.cssSelector("button[type='button']")).click();


        WebElement button = driver.findElement(By.cssSelector("#card > div > div > div > div > div > div > div:nth-child(2) > a > button"));
        button.click();

        try {
            TimeUnit.SECONDS.sleep(1);
            element = driver.findElement(By.cssSelector("#naziv"));
            element.clear();
            element.sendKeys("Nova djelatnost");


            button = driver.findElement(By.cssSelector("body > app-root > div > app-usluzna-djelatnost-form > div > form > button"));
            button.click();
            WebElement labela = driver.findElement(By.cssSelector("body > app-root > div > app-usluzna-djelatnost-form > div > form > p"));
            assertEquals("Podatci uspješno spremljeni.", labela.getText().trim());
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}

