package hr.fer.proinz.radnovrijeme.tests;

import java.util.concurrent.TimeUnit;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginTests {

    @Test
    public void testLoginGoodCreds() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://nabildanim0zak.herokuapp.com/login");


        WebElement element = driver.findElement(By.cssSelector("input[type='text']"));
        element.sendKeys("DPetrovic");

        element = driver.findElement(By.cssSelector("input[type='password']"));
        element.sendKeys("123");

        driver.findElement(By.cssSelector("button[type='button']")).click();

        String redirURL = driver.getCurrentUrl();

        boolean compRes= redirURL.contains("login");

        assertTrue(compRes);


        driver.quit();

    }

    @Test
    public void testLoginBadCreds() {

        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://nabildanim0zak.herokuapp.com/login");


        WebElement element = driver.findElement(By.cssSelector("input[type='text']"));
        element.sendKeys("testadmin");

        element = driver.findElement(By.cssSelector("input[type='password']"));
        element.sendKeys("22345");

        driver.findElement(By.cssSelector("button[type='button']")).click();

        String redirURL = driver.getCurrentUrl();

        boolean compRes= redirURL.contains("home");

        assertEquals(compRes, false);


        driver.quit();

    }

}
